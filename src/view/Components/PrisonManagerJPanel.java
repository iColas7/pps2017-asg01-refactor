package view.Components;

import java.awt.LayoutManager;
import javax.swing.JPanel;
import java.awt.Color;

public class PrisonManagerJPanel extends JPanel {

	private static final long serialVersionUID = 3177915859799100383L;
	private final static int COLOR = 210;

	/**
	 * Constructor
	 */
	public PrisonManagerJPanel() {
		super();
		this.setBackground(new Color(COLOR, COLOR, COLOR));
	}

	/**
	 * Constructor with layout
	 * 
	 * @param layout
	 *            The layout of the panel
	 */
	public PrisonManagerJPanel(final LayoutManager layout) {
		super(layout);
		this.setBackground(new Color(COLOR, COLOR, COLOR));
	}
}
