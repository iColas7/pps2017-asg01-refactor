package view.Components;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class PrisonManagerJFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4583640093618196192L;
	private static final String LOGO = "res/logo.png";

	String logoPath = LOGO;
	ImageIcon iconLogo = new ImageIcon(logoPath);

	/**
	 * Constructor
	 */
	public PrisonManagerJFrame() {
		this.setIconImage(iconLogo.getImage());
		setResizable(false);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
}
