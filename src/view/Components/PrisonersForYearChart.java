package view.Components;

import java.util.Map;

import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RefineryUtilities;

public class PrisonersForYearChart extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7145050658657438095L;
	private static final String YEAR = "Year";
	private static final String PRISONERS_NUMBER = "Prisoners' number";
	private static final int WIDTH = 560;
	private static final int HEIGHT = 367;
	private Map<Integer, Integer> mapPrisonersForYear;

	/**
	 * Constructor
	 * 
	 * @param prisonersForYear
	 *           	Map that contains dates
	 * @param title
	 *            Chart's title
	 * @param chartName
	 *            Chart's name
	 */
	public PrisonersForYearChart(final Map<Integer, Integer> prisonersForYear, final String title, final String chartName) {
		super(title);
		this.mapPrisonersForYear = prisonersForYear;
		JFreeChart barChart = ChartFactory.createBarChart(chartName, YEAR, PRISONERS_NUMBER, createDataset(),
				PlotOrientation.VERTICAL, true, true, false);
		ChartPanel chartPanel = new ChartPanel(barChart);
		chartPanel.setPreferredSize(new java.awt.Dimension(WIDTH, HEIGHT));
		setContentPane(chartPanel);
		this.pack();
		RefineryUtilities.centerFrameOnScreen(this);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	/**
	 * Method to insert dates in diagram
	 * 
	 * @return dataset
	 */
	private CategoryDataset createDataset() {
		final String name = "-";
		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		for (final Map.Entry<Integer, Integer> entry : mapPrisonersForYear.entrySet()) {
			dataset.addValue(Double.valueOf(entry.getValue()), entry.getKey(), name);
		}
		return dataset;
	}
}