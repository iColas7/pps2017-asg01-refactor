package view.Components;

import java.util.HashMap;
import java.util.Map;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.ui.RefineryUtilities;

public class CrimesPrisonersChart extends JFrame {

	private static final long serialVersionUID = -1753760388662708023L;
	private static Map<String, Integer> crimesMap = new HashMap<>();
	private static final String PERCENT_CRIMES_ACTUAL_PRISONERS = "Percent crimes from actual prisoners";
	
	/**
	 * Constructor
	 * 
	 * @param title
	 *            titolo grafico
	 * @param crimes
	 *            mappa contenente i dati
	 */
	public CrimesPrisonersChart(final String title, final Map<String, Integer> crimes) {
		super(title);
		CrimesPrisonersChart.crimesMap = crimes;
		setContentPane(createDemoPanel());
		this.setSize(600, 600);
		RefineryUtilities.centerFrameOnScreen(this);
		this.setVisible(true);
	}

	private static PieDataset createDataset() {
		DefaultPieDataset dataset = new DefaultPieDataset();
		for (final Map.Entry<String, Integer> crime : crimesMap.entrySet()) {
			dataset.setValue(String.valueOf(crime.getKey()), crime.getValue());
		}
		return dataset;
	}

	private static JFreeChart createChart(final PieDataset dataset) {
		JFreeChart chart = ChartFactory.createPieChart(PERCENT_CRIMES_ACTUAL_PRISONERS, dataset, true, true, false);
		return chart;
	}

	/**
	 * 
	 * @return demo chart
	 */
	public static JPanel createDemoPanel() {
		JFreeChart chart = createChart(createDataset());
		return new ChartPanel(chart);
	}
}
