package view.Interfaces;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SpringLayout;

import controller.Implementations.SupervisorControllerImpl.BackListener;
import controller.Implementations.SupervisorControllerImpl.InsertGuardListener;
import controller.Implementations.SupervisorControllerImpl.RemoveGuardListener;
import controller.Implementations.SupervisorControllerImpl.ShowPrisonersListener;
import controller.Implementations.SupervisorControllerImpl.ViewGuardListener;
import utils.MyUtils;
import view.Components.PrisonManagerJFrame;
import view.Components.PrisonManagerJPanel;
import view.Components.SpringUtilities;
import view.Interfaces.Inter.CommonView;
import view.Interfaces.Inter.SupervisorFunctions;

public class SupervisorFunctionsView extends PrisonManagerJFrame implements SupervisorFunctions, CommonView {

	private static final long serialVersionUID = 1240198114577795025L;
	private static final String RESERVED_FUNCTION = "Reserved function";
	private static final String ADD_GUARD = "Add guard";
	private static final String REMOVE_GUARD = "Remove guard";
	private static final String VIEW_GUARD = "View guard";
	private static final String VIEW_ALL_PRISONERS = "View all prisoners";
	private static final int WIDTH = 580;
	private static final int HEIGHT = 200;
	private static final int ROWS = 2;

	private final PrisonManagerJPanel north;
	private final JLabel title = new JLabel(RESERVED_FUNCTION);
	private final PrisonManagerJPanel center;
	private final JButton addGuard = new JButton(ADD_GUARD);
	private final JButton removeGuard = new JButton(REMOVE_GUARD);
	private final JButton viewGuard = new JButton(VIEW_GUARD);
	private final JButton viewPrisoners = new JButton(VIEW_ALL_PRISONERS);
	private final PrisonManagerJPanel south;
	private final JButton back = new JButton(MyUtils.getBack());
	private int rank;

	private SupervisorFunctionsView(final int rank) {
		this.rank = rank;
		this.setSize(WIDTH, HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		this.north = new PrisonManagerJPanel(new FlowLayout());
		this.north.add(title);
		this.getContentPane().add(BorderLayout.NORTH, north);
		this.center = new PrisonManagerJPanel(new SpringLayout());
		this.center.add(addGuard);
		this.center.add(removeGuard);
		this.center.add(viewGuard);
		this.center.add(viewPrisoners);
		SpringUtilities.makeCompactGrid(center, ROWS, ROWS, MyUtils.getPosition(), MyUtils.getPosition(),
				MyUtils.getPosition(), MyUtils.getPosition());
		this.getContentPane().add(BorderLayout.CENTER, center);
		this.south = new PrisonManagerJPanel(new FlowLayout());
		this.south.add(back);
		this.getContentPane().add(BorderLayout.SOUTH, south);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public static SupervisorFunctionsView createSupervisorFunctionsView(final int rank) {
		return new SupervisorFunctionsView(rank);
	}

	@Override
	public int getRank() {
		return rank;
	}

	@Override
	public void addBackListener(BackListener backListener) {
		back.addActionListener(backListener);
	}

	@Override
	public void addShowPrisonersListener(ShowPrisonersListener showPrisonersListeners) {
		viewPrisoners.addActionListener(showPrisonersListeners);
	}

	@Override
	public void addInsertGuardListener(InsertGuardListener insertGaurdListener) {
		addGuard.addActionListener(insertGaurdListener);
	}

	@Override
	public void addRemoveGuardListener(RemoveGuardListener removeListener) {
		removeGuard.addActionListener(removeListener);
	}

	@Override
	public void addviewGuardListener(ViewGuardListener viewListener) {
		viewGuard.addActionListener(viewListener);
	}

	@Override
	public void displayErrorMessage(String error) {
		JOptionPane.showMessageDialog(this, error);
	}
}
