package view.Interfaces;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import controller.Implementations.RemovePrisonerControllerImpl.BackListener;
import controller.Implementations.RemovePrisonerControllerImpl.RemoveListener;
import utils.MyUtils;
import view.Components.PrisonManagerJFrame;
import view.Components.PrisonManagerJPanel;
import view.Interfaces.Inter.CommonView;
import view.Interfaces.Inter.RemovePrisoner;

public class RemovePrisonerView extends PrisonManagerJFrame implements RemovePrisoner, CommonView {

	private static final long serialVersionUID = 8578870382924181404L;
	private static final String REMOVE = "Remove";
	private static final int WIDTH = 450;
	private static final int HEIGHT = 120;

	private final PrisonManagerJPanel center;
	private final JButton remove = new JButton(REMOVE);
	private final JButton back = new JButton(MyUtils.getBack());
	private final PrisonManagerJPanel north;
	private final JLabel prisonerID = new JLabel(MyUtils.getPrisonerId());
	private final JTextField inputPrisonerID = new JTextField(2);
	private int rank;

	private RemovePrisonerView(final int rank) {
		this.rank = rank;
		this.setSize(WIDTH, HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		this.north = new PrisonManagerJPanel(new FlowLayout());
		this.north.add(prisonerID);
		this.north.add(inputPrisonerID);
		this.getContentPane().add(BorderLayout.NORTH, north);
		this.center = new PrisonManagerJPanel(new FlowLayout());
		this.center.add(remove);
		this.center.add(back);
		this.getContentPane().add(BorderLayout.CENTER, center);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public static RemovePrisonerView createRemovePrisonerView(final int rank) {
		return new RemovePrisonerView(rank);
	}

	@Override
	public void addRemoveListener(RemoveListener removeListener) {
		remove.addActionListener(removeListener);
	}

	@Override
	public void addBackListener(BackListener backListener) {
		back.addActionListener(backListener);
	}

	@Override
	public int getID() {
		if (inputPrisonerID.getText().equals(""))
			return -1;
		return Integer.valueOf(inputPrisonerID.getText());
	}

	@Override
	public void displayErrorMessage(String error) {
		JOptionPane.showMessageDialog(this, error);
	}

	@Override
	public int getRank() {
		return this.rank;
	}
}
