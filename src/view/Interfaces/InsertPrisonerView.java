package view.Interfaces;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import controller.Implementations.InsertPrisonerControllerImpl.AddCrimeListener;
import controller.Implementations.InsertPrisonerControllerImpl.BackListener;
import controller.Implementations.InsertPrisonerControllerImpl.InsertPrisonerListener;
import utils.MyUtils;
import view.Components.PrisonManagerJFrame;
import view.Components.PrisonManagerJPanel;
import view.Components.SpringUtilities;
import view.Interfaces.Inter.CommonView;
import view.Interfaces.Inter.InsertPrisoner;

public class InsertPrisonerView extends PrisonManagerJFrame implements InsertPrisoner, CommonView {

	private static final long serialVersionUID = -3914632428464622887L;
	private static final String START_PRISONING = MyUtils.getStartPrisoning() + " " + MyUtils.getDataPattern();
	private static final String END_PRISONING = MyUtils.getEndPrisoning() + " " + MyUtils.getDataPattern();
	private static final String BIRTHDAY = "Birthday " + MyUtils.getDataPattern();
	private static final String ADD_CRIME = "Add crime";
	private static final int WIDTH = 750;
	private static final int HEIGHT = 400;
	private static final int ROW_EAST = 3;
	private static final int COLUMNS_EAST = 1;
	private static final int ROW_CENTER = 7;
	private static final int COLUMNS_CENTER = 2;
	private static final int LENGTH_DATE = 8;

	private final PrisonManagerJPanel south;
	private final JButton insert = new JButton(MyUtils.getInsert());
	private final PrisonManagerJPanel north;
	private final JLabel prisonerID = new JLabel(MyUtils.getPrisonerId());
	private final JTextField inputPrisonerID = new JTextField(MyUtils.getPosition());
	private final JLabel prisonerName = new JLabel(MyUtils.getName());
	private final JTextField inputPrisonerName = new JTextField(MyUtils.getPosition());
	private final JLabel prisonerSurname = new JLabel(MyUtils.getSurname());
	private final JTextField inputPrisonerSurname = new JTextField(MyUtils.getPosition());
	private final JLabel prisonerBirthday = new JLabel(BIRTHDAY);
	private final JTextField inputPrisonerBirthday = new JTextField(MyUtils.getPosition());
	private final PrisonManagerJPanel east;
	private final PrisonManagerJPanel center;
	private final JLabel startPrisoning = new JLabel(START_PRISONING);
	private final JTextField inputStartPrisoning = new JTextField(LENGTH_DATE);
	private final JLabel endPrisoning = new JLabel(END_PRISONING);
	private final JTextField inputEndPrisoning = new JTextField(LENGTH_DATE);
	private final JLabel cellID = new JLabel(MyUtils.getCellId());
	private final JTextField inputCellID = new JTextField(LENGTH_DATE);
	private final JButton back = new JButton(MyUtils.getBack());
	private final JLabel titlePrisonerView = new JLabel(MyUtils.getPrisonerInserted());
	private final JComboBox<?> allPossibleCrimes;
	private final JTextArea viewAllCrimes;
	private final JButton addCrime = new JButton(ADD_CRIME);
	private final String[] crimes = { MyUtils.getCrimesAgainstAnimals(), MyUtils.getCrimesAssociatives(), MyUtils.getBlasphemyAndSacrilege(),
			MyUtils.getFinancialAndEconomicsCrimes(), MyUtils.getFalseTestimony(), MyUtils.getMilitaryCrimes(), MyUtils.getHeritageCrimes(), MyUtils.getPersonCrimes(),
			MyUtils.getSystemCrimes(), MyUtils.getTaxCrimes(), MyUtils.getDrugCrimes(), MyUtils.getScamCases() };
	private String dataPattern = MyUtils.getDataPattern();
	private SimpleDateFormat format = new SimpleDateFormat(dataPattern);
	private Date date;
	private int rank;

	private InsertPrisonerView(final int rank) {
		this.rank = rank;
		this.setSize(WIDTH, HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		this.north = new PrisonManagerJPanel(new FlowLayout());
		this.north.add(titlePrisonerView);
		this.getContentPane().add(BorderLayout.NORTH, north);
		this.allPossibleCrimes = new JComboBox<String>(crimes);
		this.allPossibleCrimes.setSelectedItem(0);
		this.viewAllCrimes = new JTextArea();
		this.viewAllCrimes.setEditable(false);
		this.east = new PrisonManagerJPanel(new SpringLayout());
		this.east.add(allPossibleCrimes);
		this.east.add(viewAllCrimes);
		JScrollPane logScrollPane = new JScrollPane(addCrime);
		this.east.add(logScrollPane);
		SpringUtilities.makeCompactGrid(east, ROW_EAST, COLUMNS_EAST, MyUtils.getPosition(), MyUtils.getPosition(),
				MyUtils.getPosition(), MyUtils.getPosition());
		this.getContentPane().add(BorderLayout.EAST, east);
		this.center = new PrisonManagerJPanel(new SpringLayout());
		this.center.add(prisonerID);
		this.center.add(inputPrisonerID);
		this.inputPrisonerID.setText("0");
		this.center.add(prisonerName);
		this.center.add(inputPrisonerName);
		this.center.add(prisonerSurname);
		this.center.add(inputPrisonerSurname);
		this.center.add(prisonerBirthday);
		this.center.add(inputPrisonerBirthday);
		this.inputPrisonerBirthday.setText(MyUtils.getDefaultData());
		this.center.add(startPrisoning);
		this.center.add(inputStartPrisoning);
		this.inputStartPrisoning.setText(MyUtils.getNewYearData());
		this.center.add(endPrisoning);
		this.center.add(inputEndPrisoning);
		this.inputEndPrisoning.setText(MyUtils.getNewYearData());
		this.center.add(cellID);
		this.center.add(inputCellID);
		this.inputCellID.setText("0");
		SpringUtilities.makeCompactGrid(center, ROW_CENTER, COLUMNS_CENTER, MyUtils.getPosition(),
				MyUtils.getPosition(), MyUtils.getPosition(), MyUtils.getPosition());
		this.getContentPane().add(BorderLayout.CENTER, center);
		this.south = new PrisonManagerJPanel(new FlowLayout());
		this.south.add(insert);
		this.south.add(back);
		this.getContentPane().add(BorderLayout.SOUTH, south);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public static InsertPrisonerView createInsertPrisonerView(final int rank) {
		return new InsertPrisonerView(rank);
	}

	@Override
	public void addInsertPrisonerListener(final InsertPrisonerListener addPrisonerListener) {
		insert.addActionListener(addPrisonerListener);
	}

	@Override
	public void addBackListener(final BackListener backListener) {
		back.addActionListener(backListener);
	}

	@Override
	public int getPrisonerID() {
		return Integer.valueOf(inputPrisonerID.getText());
	}

	@Override
	public String getPrisonerName() {
		return inputPrisonerName.getText();
	}

	@Override
	public String getPrisonerSurname() {
		return inputPrisonerSurname.getText();
	}

	@Override
	public Date getStartPrisoning() {
		try {
			date = format.parse(inputStartPrisoning.getText());
		} catch (final ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	@Override
	public Date getEndPrisoning() {
		try {
			date = format.parse(inputEndPrisoning.getText());
		} catch (final ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	@Override
	public Date getPrisonerBirthday() {
		try {
			date = format.parse(inputPrisonerBirthday.getText());
		} catch (final ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	@Override
	public int getCellID() {
		return Integer.valueOf(this.inputCellID.getText());
	}

	@Override
	public void displayErrorMessage(final String error) {
		JOptionPane.showMessageDialog(this, error);
	}

	@Override
	public int getRank() {
		return this.rank;
	}

	@Override
	public void setCrimesList(final List<String> crimesList) {
		viewAllCrimes.setText("");
		for (final String crime : crimesList) {
			viewAllCrimes.append(crime + "\n");
		}
	}

	@Override
	public List<String> getCrimesList() {
		String crimeList[] = viewAllCrimes.getText().split("\\r?\\n");
		ArrayList<String> finalCrimesList = new ArrayList<>(Arrays.asList(crimeList));
		return finalCrimesList;
	}

	@Override
	public String getCrimeSelected() {
		return allPossibleCrimes.getSelectedItem().toString();
	}

	@Override
	public void addAddCrimeListener(final AddCrimeListener addCrimeListener) {
		addCrime.addActionListener(addCrimeListener);
	}

}
