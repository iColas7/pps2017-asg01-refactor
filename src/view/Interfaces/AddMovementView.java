package view.Interfaces;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import controller.Implementations.AddMovementControllerImpl.BackListener;
import controller.Implementations.AddMovementControllerImpl.InsertListener;
import utils.MyUtils;
import view.Components.PrisonManagerJFrame;
import view.Components.PrisonManagerJPanel;
import view.Components.SpringUtilities;
import view.Interfaces.Inter.*;

public class AddMovementView extends PrisonManagerJFrame implements AddMovement, CommonView {

	private static final long serialVersionUID = -3724774108126619974L;
	private static final int WIDTH = 250;
	private static final int HEIGHT = 300;
	private static final int ROWS = 3;
	private static final int COLUMNS = 2;
	private static final int POSITION = 6;

	private final String[] movementSymbol = { MyUtils.getPlus(), MyUtils.getMinus() };
	private final PrisonManagerJPanel center;
	private final PrisonManagerJPanel south;
	private final PrisonManagerJPanel north;
	private final JLabel panelTitle = new JLabel(MyUtils.getAddMovement());
	private final JLabel amount = new JLabel(MyUtils.getAmount());
	private final JLabel symbol = new JLabel(MyUtils.getPlus() + " : " + MyUtils.getMinus());
	private final JLabel description = new JLabel(MyUtils.getDescription());
	private final JTextField amountInput = new JTextField(POSITION);
	private final JTextField descriptionInput = new JTextField(POSITION);
	private final JComboBox<?> symbolValues;
	private final JButton back = new JButton(MyUtils.getBack());
	private final JButton insert = new JButton(MyUtils.getInsertMovement());
	private int guardRank;

	private AddMovementView(final int rank) {
		this.guardRank = rank;
		this.setSize(WIDTH, HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		this.north = new PrisonManagerJPanel(new FlowLayout());
		this.north.add(panelTitle);
		this.getContentPane().add(BorderLayout.NORTH, north);
		this.center = new PrisonManagerJPanel(new SpringLayout());
		this.symbolValues = new JComboBox<String>(movementSymbol);
		this.symbolValues.setSelectedItem(1);
		this.center.add(symbol);
		this.center.add(symbolValues);
		this.center.add(amount);
		this.center.add(amountInput);
		this.center.add(description);
		this.center.add(descriptionInput);
		SpringUtilities.makeCompactGrid(center, ROWS, COLUMNS, POSITION, POSITION, POSITION, POSITION);
		this.getContentPane().add(BorderLayout.CENTER, center);
		this.south = new PrisonManagerJPanel(new FlowLayout());
		this.south.add(insert);
		this.south.add(back);
		this.getContentPane().add(BorderLayout.SOUTH, south);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public static AddMovementView createAddMovementView(final int rank) {
		return new AddMovementView(rank);
	}

	@Override
	public int getRank() {
		return this.guardRank;
	}

	@Override
	public void addBackListener(final BackListener backListener) {
		back.addActionListener(backListener);
	}

	@Override
	public void addInsertListener(final InsertListener insertListener) {
		insert.addActionListener(insertListener);
	}

	@Override
	public String getMovementDescription() {
		return descriptionInput.getText();
	}

	@Override
	public Double getMovementValue() {
		return Double.valueOf(amountInput.getText());
	}

	@Override
	public String getMovementSymbol() {
		return String.valueOf(symbolValues.getSelectedItem());
	}

	@Override
	public void displayErrorMessage(final String error) {
		JOptionPane.showMessageDialog(this, error);
	}

}
