package view.Interfaces;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import controller.Implementations.ViewGuardControllerImpl.BackListener;
import controller.Implementations.ViewGuardControllerImpl.ViewGuardListener;
import utils.MyUtils;
import view.Components.PrisonManagerJFrame;
import view.Components.PrisonManagerJPanel;
import view.Components.SpringUtilities;
import view.Interfaces.Inter.CommonView;
import view.Interfaces.Inter.ViewGuard;

public class ViewGuardView extends PrisonManagerJFrame implements ViewGuard, CommonView {

	private static final long serialVersionUID = 86423038519594644L;
	private static final String VIEW_PROFILE = "View profile";
	private static final String RANK = "Rank";
	private static final int WIDTH = 500;
	private static final int HEIGHT = 230;
	private static final int ROWS = 5;
	private static final int COLUMNS = 2;

	private final PrisonManagerJPanel south;
	private final JButton view = new JButton(VIEW_PROFILE);
	private final JButton back = new JButton(MyUtils.getBack());
	private final PrisonManagerJPanel north;
	private final JLabel guardID = new JLabel(MyUtils.getGuardId());
	private final JTextField inputGuardID = new JTextField(2);
	private final JLabel name;
	private final JLabel guardName;
	private final JLabel surname;
	private final JLabel guardSurname;
	private final JLabel birthday;
	private final JLabel guardBirthday;
	private final JLabel labelRank;
	private final JLabel guardRank;
	private final JLabel telephone;
	private final JLabel guardTelephone;
	private final PrisonManagerJPanel center;
	private int actualGuardRank;
	
	private ViewGuardView(final int rank) {
		this.actualGuardRank = rank;
		this.getContentPane().setLayout(new BorderLayout());
		this.center = new PrisonManagerJPanel(new SpringLayout());
		this.name = new JLabel(MyUtils.getName());
		this.guardName = new JLabel();
		this.center.add(name);
		this.center.add(guardName);
		this.surname = new JLabel(MyUtils.getSurname());
		this.guardSurname = new JLabel();
		this.center.add(surname);
		this.center.add(guardSurname);
		this.birthday = new JLabel(MyUtils.getBirthday());
		this.guardBirthday = new JLabel();
		this.center.add(birthday);
		this.center.add(guardBirthday);
		this.labelRank = new JLabel(RANK);
		this.guardRank = new JLabel();
		this.center.add(labelRank);
		this.center.add(guardRank);
		this.telephone = new JLabel(MyUtils.getTelephoneNumber());
		this.guardTelephone = new JLabel();
		this.center.add(telephone);
		this.center.add(guardTelephone);
		SpringUtilities.makeCompactGrid(center, ROWS, COLUMNS, MyUtils.getPosition(), MyUtils.getPosition(),
				MyUtils.getPosition(), MyUtils.getPosition());
		this.getContentPane().add(BorderLayout.CENTER, center);
		this.north = new PrisonManagerJPanel(new FlowLayout());
		this.north.add(guardID);
		this.north.add(inputGuardID);
		this.getContentPane().add(BorderLayout.NORTH, north);
		this.south = new PrisonManagerJPanel(new FlowLayout());
		this.south.add(view);
		this.south.add(back);
		this.getContentPane().add(BorderLayout.SOUTH, south);
		this.setSize(WIDTH, HEIGHT);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public static ViewGuardView createViewGuardView(final int rank) {
		return new ViewGuardView(rank);
	}

	@Override
	public int getRank() {
		return this.actualGuardRank;
	}

	@Override
	public void addViewListener(final ViewGuardListener viewListener) {
		view.addActionListener(viewListener);
	}

	@Override
	public void addBackListener(final BackListener backListener) {
		back.addActionListener(backListener);
	}

	@Override
	public void displayErrorMessage(final String error) {
		JOptionPane.showMessageDialog(this, error);
	}

	@Override
	public int getID() {
		if (inputGuardID.getText().equals(""))
			return -1;
		return Integer.valueOf(inputGuardID.getText());
	}

	@Override
	public void setName(final String name) {
		this.guardName.setText(name);
	}

	@Override
	public void setSurname(final String surname) {
		this.guardSurname.setText(surname);
	}

	@Override
	public void setBirth(final String birthdate) {
		this.guardBirthday.setText(birthdate);
	}

	@Override
	public void setRank(final String rank) {
		this.guardRank.setText(rank);
	}

	@Override
	public void setTelephone(final String telephone) {
		this.guardTelephone.setText(telephone);
	}

}
