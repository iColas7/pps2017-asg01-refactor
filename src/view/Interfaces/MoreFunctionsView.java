package view.Interfaces;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SpringLayout;

import controller.Implementations.MoreFunctionsControllerImpl.AddMovementListener;
import controller.Implementations.MoreFunctionsControllerImpl.AddPolicemanListener;
import controller.Implementations.MoreFunctionsControllerImpl.AddVisitorsListener;
import controller.Implementations.MoreFunctionsControllerImpl.BackListener;
import controller.Implementations.MoreFunctionsControllerImpl.BalanceListener;
import controller.Implementations.MoreFunctionsControllerImpl.OpenChartPrisonersForYear;
import controller.Implementations.MoreFunctionsControllerImpl.OpenChartPercentCrimes;
import controller.Implementations.MoreFunctionsControllerImpl.ViewCellsListener;
import controller.Implementations.MoreFunctionsControllerImpl.ViewPolicemanListener;
import controller.Implementations.MoreFunctionsControllerImpl.ViewVisitorsListener;
import view.Components.PrisonManagerJFrame;
import view.Components.PrisonManagerJPanel;
import view.Components.SpringUtilities;
import view.Interfaces.Inter.CommonView;
import view.Interfaces.Inter.MoreFunctions;

public class MoreFunctionsView extends PrisonManagerJFrame implements MoreFunctions, CommonView {

	private static final long serialVersionUID = -960406183837784254L;
	private static final String OTHER_FUNCTIONS = "Other functions";
	private static final String ADD_MOVEMENT = "Add movement";
	private static final String VIEW_BALANCE = "View balance";
	private static final String PRISONERS_FOR_YEAR = "Chart prisoners for year";
	private static final String CRIMES_PERCENT = "Chart crimes percent";
	private static final String ADD_VISITOR = "Add visitor";
	private static final String ADD_POLICEMAN = "Add policeman";
	private static final String VIEW_VISITORS = "View visitors";
	private static final String VIEW_POLICEMAN = "View policeman";
	private static final String VIEW_CELLS = "View cells";
	private static final String BACK = "Back";
	private static final int WIDTH = 500;
	private static final int HEIGHT = 350;
	private static final int ROWS = 4;
	private static final int COLUMNS = 2;
	private static final int POSITION = 6;
	
	private final PrisonManagerJPanel northPanel;
	private final JLabel titleView = new JLabel(OTHER_FUNCTIONS);
	private final PrisonManagerJPanel centerPanel;
	private final JButton addMovement = new JButton(ADD_MOVEMENT);
	private final JButton viewBalance = new JButton(VIEW_BALANCE);
	private final JButton viewChartPrisonersForYear = new JButton(PRISONERS_FOR_YEAR);
	private final JButton viewChartCrimesPercent = new JButton(CRIMES_PERCENT);
	private final JButton addVisitors = new JButton(ADD_VISITOR);
	private final JButton addPoliceman = new JButton(ADD_POLICEMAN);
	private final JButton viewVisitors = new JButton(VIEW_VISITORS);
	private final JButton viewPoliceman = new JButton(VIEW_POLICEMAN);
	private final JButton viewCells = new JButton(VIEW_CELLS);
	private final PrisonManagerJPanel southPanel;
	private final JButton back = new JButton(BACK);
	private int guardRank;

	private MoreFunctionsView(final int rank) {
		this.guardRank = rank;
		this.setSize(WIDTH, HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		this.northPanel = new PrisonManagerJPanel(new FlowLayout());
		this.northPanel.add(titleView);
		this.getContentPane().add(BorderLayout.NORTH, northPanel);
		this.centerPanel = new PrisonManagerJPanel(new SpringLayout());
		this.centerPanel.add(addMovement);
		this.centerPanel.add(viewBalance);
		this.centerPanel.add(viewChartPrisonersForYear);
		this.centerPanel.add(viewChartCrimesPercent);
		this.centerPanel.add(addVisitors);
		this.centerPanel.add(viewVisitors);
		this.centerPanel.add(viewCells);
		this.centerPanel.add(addPoliceman);
		SpringUtilities.makeCompactGrid(centerPanel, ROWS, COLUMNS, POSITION, POSITION, POSITION, POSITION);
		this.getContentPane().add(BorderLayout.CENTER, centerPanel);
		this.southPanel = new PrisonManagerJPanel(new FlowLayout());
		this.southPanel.add(viewPoliceman);
		this.southPanel.add(back);
		this.getContentPane().add(BorderLayout.SOUTH, southPanel);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public static MoreFunctionsView createMoreFunctionsView(final int rank) {
		return new MoreFunctionsView(rank);
	}

	@Override
	public int getRank() {
		return this.guardRank;
	}

	@Override
	public void addBackListener(final BackListener backListener) {
		back.addActionListener(backListener);
	}

	@Override
	public void addAddMovementListener(final AddMovementListener addMovementListener) {
		addMovement.addActionListener(addMovementListener);
	}

	@Override
	public void addBalanceListener(final BalanceListener balanceListener) {
		viewBalance.addActionListener(balanceListener);
	}

	@Override
	public void addChartPrisonersForYear(final OpenChartPrisonersForYear prisonerForYear) {
		viewChartPrisonersForYear.addActionListener(prisonerForYear);
	}

	@Override
	public void addChartCrimesPercent(final OpenChartPercentCrimes crimesPercent) {
		viewChartCrimesPercent.addActionListener(crimesPercent);
	}

	@Override
	public void addAddVisitorsListener(final AddVisitorsListener addVisitorsListener) {
		addVisitors.addActionListener(addVisitorsListener);
	}

	@Override
	public void addViewVisitorsListener(final ViewVisitorsListener viewVisitorsListener) {
		viewVisitors.addActionListener(viewVisitorsListener);
	}
	
	@Override
	public void addAddPolicemanListener(final AddPolicemanListener addPolicemanListener) {
		addPoliceman.addActionListener(addPolicemanListener);
	}

	@Override
	public void addViewPolicemanListener(final ViewPolicemanListener viewPolicemanListener) {
		viewPoliceman.addActionListener(viewPolicemanListener);
	}

	@Override
	public void addViewCellsListener(final ViewCellsListener viewCellsListener) {
		viewCells.addActionListener(viewCellsListener);
	}

	@Override
	public void displayErrorMessage(final String error) {
		JOptionPane.showMessageDialog(this, error);
	}
}
