package view.Interfaces;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import controller.Implementations.MainControllerImpl.InsertPrisonerListener;
import controller.Implementations.MainControllerImpl.LogoutListener;
import controller.Implementations.MainControllerImpl.MoreFunctionsListener;
import controller.Implementations.MainControllerImpl.RemovePrisonerListener;
import controller.Implementations.MainControllerImpl.SupervisorListener;
import controller.Implementations.MainControllerImpl.ViewPrisonerListener;
import utils.MyUtils;
import view.Components.PrisonManagerJFrame;
import view.Components.PrisonManagerJPanel;
import view.Interfaces.Inter.CommonView;
import view.Interfaces.Inter.Main;

public class MainView extends PrisonManagerJFrame implements Main, CommonView {

	private static final long serialVersionUID = -2585136897389059255L;
	private static final String ADD_PRISONER = "Add prisoner";
	private static final String REMOVE_PRISONER = "Remove prisoner";
	private static final String VIEW_PRISONER_PROFILE = "View prisoner profile";
	private static final String RESERVED_FUNCTIONS = "Reserved functions (Rank 3)";
	private static final String OTHER_FUNCTIONS = "Other functions (Rank 2)";
	private static final String LOGOUT = "Logout";
	private static final int WIDTH = 550;
	private static final int HEIGHT = 150;

	private final PrisonManagerJPanel center;
	private final JButton addPrisoner = new JButton(ADD_PRISONER);
	private final JButton removePrisoner = new JButton(REMOVE_PRISONER);;
	private final JButton viewPrisoner = new JButton(VIEW_PRISONER_PROFILE);
	private final PrisonManagerJPanel south;
	private final JButton highRankOnly = new JButton(RESERVED_FUNCTIONS);
	private final JButton moreFunctions = new JButton(OTHER_FUNCTIONS);
	private final JButton logout = new JButton(LOGOUT);
	private final PrisonManagerJPanel north;
	private final JLabel title = new JLabel(MyUtils.getTitleApp());
	private int rank;

	private MainView(final int rank) {
		this.rank = rank;
		this.setSize(WIDTH, HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		this.center = new PrisonManagerJPanel(new FlowLayout());
		this.center.add(addPrisoner);
		this.center.add(removePrisoner);
		this.center.add(viewPrisoner);
		this.getContentPane().add(BorderLayout.CENTER, center);
		this.south = new PrisonManagerJPanel(new FlowLayout());
		this.south.add(moreFunctions);
		this.south.add(highRankOnly);
		this.south.add(logout);
		if (rank < 2) {
			moreFunctions.setEnabled(false);
		}
		if (rank < 3) {
			highRankOnly.setEnabled(false);
		}
		this.getContentPane().add(BorderLayout.SOUTH, south);
		this.north = new PrisonManagerJPanel(new FlowLayout());
		this.north.add(title);
		this.getContentPane().add(BorderLayout.NORTH, north);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public static MainView createMainView(final int rank) {
		return new MainView(rank);
	}

	@Override
	public void addLogoutListener(final LogoutListener logoutListener) {
		logout.addActionListener(logoutListener);
	}

	@Override
	public void addInsertPrisonerListener(final InsertPrisonerListener insertPrisonerListener) {
		addPrisoner.addActionListener(insertPrisonerListener);
	}

	@Override
	public void addRemovePrisonerListener(final RemovePrisonerListener removePrisonerListener) {
		removePrisoner.addActionListener(removePrisonerListener);
	}

	@Override
	public void addViewPrisonerListener(final ViewPrisonerListener viewPrisonerListener) {
		viewPrisoner.addActionListener(viewPrisonerListener);
	}
	
	@Override
	public void addMoreFunctionsListener(final MoreFunctionsListener moreFListener) {
		moreFunctions.addActionListener(moreFListener);
	}

	@Override
	public void addSupervisorListener(final SupervisorListener supervisorListener) {
		highRankOnly.addActionListener(supervisorListener);
	}

	@Override
	public int getRank() {
		return this.rank;
	}

	@Override
	public void displayErrorMessage(final String error) {
		JOptionPane.showMessageDialog(this, error);
	}
}
