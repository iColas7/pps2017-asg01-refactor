package view.Interfaces;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import controller.Implementations.ViewPolicemenControllerImpl.BackListener;
import utils.MyUtils;
import view.Components.PrisonManagerJFrame;
import view.Components.PrisonManagerJPanel;
import view.Interfaces.Inter.CommonView;
import view.Interfaces.Inter.ViewPolicemen;

public class ViewPolicemanView extends PrisonManagerJFrame implements ViewPolicemen, CommonView {

	private static final long serialVersionUID = -2336420746515571993L;
	private static final String VIEW_POLICEMEN = "View policemen";
	private static final int WIDTH = 540;
	private static final int HEIGHT = 440;
	private static final int WIDTH_TABLE = 500;
	private static final int HEIGHT_TABLE = 300;

	private int rank;
	private final PrisonManagerJPanel center;
	private final PrisonManagerJPanel south;
	private final PrisonManagerJPanel north;
	private final JButton back = new JButton(MyUtils.getBack());
	private final JLabel title = new JLabel(VIEW_POLICEMEN);
	private JTable policemenTable = new JTable();

	private ViewPolicemanView(final int rank) {
		this.rank = rank;
		this.setSize(WIDTH, HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		this.north = new PrisonManagerJPanel(new FlowLayout());
		this.north.add(title);
		this.getContentPane().add(BorderLayout.NORTH, north);
		this.center = new PrisonManagerJPanel(new FlowLayout());
		this.getContentPane().add(BorderLayout.CENTER, center);
		this.south = new PrisonManagerJPanel(new FlowLayout());
		this.south.add(back);
		this.getContentPane().add(BorderLayout.SOUTH, south);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public static ViewPolicemanView createViewPolicemanView(final int rank) {
		return new ViewPolicemanView(rank);
	}

	@Override
	public int getRank() {
		return this.rank;
	}

	@Override
	public void addBackListener(final BackListener backListener) {
		back.addActionListener(backListener);
	}

	@Override
	public void createPolicemenTable(final JTable policemenTable) {
		this.policemenTable = policemenTable;
		this.policemenTable.setPreferredScrollableViewportSize(new Dimension(WIDTH_TABLE, HEIGHT_TABLE));
		this.policemenTable.setFillsViewportHeight(true);
		JScrollPane scrollPane = new JScrollPane(policemenTable);
		scrollPane.setVisible(true);
		this.center.add(scrollPane);
	}

	@Override
	public void displayErrorMessage(final String error) {
		JOptionPane.showMessageDialog(this, error);
	}

}
