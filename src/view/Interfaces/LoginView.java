package view.Interfaces;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import controller.Implementations.LoginControllerImpl.LoginListener;
import utils.MyUtils;
import view.Components.PrisonManagerJFrame;
import view.Components.PrisonManagerJPanel;
import view.Interfaces.Inter.CommonView;
import view.Interfaces.Inter.Login;

public class LoginView extends PrisonManagerJFrame implements Login, CommonView {

	private static final long serialVersionUID = -9055948983228935131L;
	private static final String LOGIN = "Login";
	private static final int FIELD_POSITION = 8;
	private static final int WIDTH = 400;
	private static final int HEIGHT = 130;

	private final PrisonManagerJPanel south;
	private final JButton login = new JButton(LOGIN);
	private final PrisonManagerJPanel center;
	private final JLabel username = new JLabel(MyUtils.getUsername());
	private final JTextField inputUsername = new JTextField(FIELD_POSITION);
	private final JLabel password = new JLabel(MyUtils.getPassword());
	private final JPasswordField inputPassword = new JPasswordField(FIELD_POSITION);
	private final PrisonManagerJPanel north;
	private final JLabel title = new JLabel(MyUtils.getTitleApp());

	private LoginView() {
		this.setSize(WIDTH, HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		this.south = new PrisonManagerJPanel(new FlowLayout());
		this.south.add(login);
		this.getContentPane().add(BorderLayout.SOUTH, south);
		this.center = new PrisonManagerJPanel(new FlowLayout());
		this.center.add(username);
		this.center.add(inputUsername);
		this.center.add(password);
		this.center.add(inputPassword);
		this.getContentPane().add(BorderLayout.CENTER, center);
		this.north = new PrisonManagerJPanel(new FlowLayout());
		this.north.add(title);
		this.getContentPane().add(BorderLayout.NORTH, north);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public static LoginView createLoginView() {
		return new LoginView();
	}

	@Override
	public String getUsername() {
		return inputUsername.getText();
	}

	@Override
	public String getPassword() {
		return new String(inputPassword.getPassword());
	}

	@Override
	public void displayErrorMessage(final String error) {
		JOptionPane.showMessageDialog(this, error);
	}

	@Override
	public void addLoginListener(final LoginListener loginListener) {
		login.addActionListener(loginListener);
	}

	@Override
	public int getRank() {
		return 0;
	}

	@Override
	public void setPassword() {
		this.inputPassword.setText("");

	}

	@Override
	public void setUsername() {
		this.inputUsername.setText("");
	}
}
