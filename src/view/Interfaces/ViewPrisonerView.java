package view.Interfaces;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import controller.Implementations.ViewPrisonerControllerImpl.BackListener;
import controller.Implementations.ViewPrisonerControllerImpl.ViewProfileListener;
import utils.MyUtils;
import view.Components.PrisonManagerJFrame;
import view.Components.PrisonManagerJPanel;
import view.Components.SpringUtilities;
import view.Interfaces.Inter.CommonView;
import view.Interfaces.Inter.ViewPrisoner;

public class ViewPrisonerView extends PrisonManagerJFrame implements ViewPrisoner, CommonView {

	private static final long serialVersionUID = 7065438206105719545L;
	private static final String VIEW_PROFILE = "View profile";
	private static final int WIDTH = 550;
	private static final int HEIGHT = 350;
	private static final int ROWS = 5;
	private static final int COLUMNS = 2;

	private final PrisonManagerJPanel south;
	private final JButton view = new JButton(VIEW_PROFILE);
	private final JButton back = new JButton(MyUtils.getBack());
	private final PrisonManagerJPanel north;
	private final JLabel prisonerID = new JLabel(MyUtils.getPrisonerId());
	private final JTextField inputPrisonerID = new JTextField(2);
	private final JLabel name;
	private final JLabel prisonerName;
	private final JLabel surname;
	private final JLabel prisonerSurname;
	private final JLabel birthday;
	private final JLabel prisonerBirthday;
	private final JLabel startPrisoning;
	private final JLabel prisonerStartPrisoning;
	private final JLabel endPrisoning;
	private final JLabel prisonerEndPrisoning;
	private final PrisonManagerJPanel center;
	private final PrisonManagerJPanel east;
	private final JTextArea textArea;
	private int rank;

	private ViewPrisonerView(final int rank) {
		this.rank = rank;
		this.setSize(WIDTH, HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		this.center = new PrisonManagerJPanel(new SpringLayout());
		this.name = new JLabel(MyUtils.getName());
		this.prisonerName = new JLabel();
		this.center.add(name);
		this.center.add(prisonerName);
		this.surname = new JLabel(MyUtils.getSurname());
		this.prisonerSurname = new JLabel();
		this.center.add(surname);
		this.center.add(prisonerSurname);
		this.birthday = new JLabel(MyUtils.getBirthday());
		this.prisonerBirthday = new JLabel();
		this.center.add(birthday);
		this.center.add(prisonerBirthday);
		this.startPrisoning = new JLabel(MyUtils.getStartPrisoning());
		this.prisonerStartPrisoning = new JLabel();
		this.center.add(startPrisoning);
		this.center.add(prisonerStartPrisoning);
		this.endPrisoning = new JLabel(MyUtils.getEndPrisoning());
		this.prisonerEndPrisoning = new JLabel();
		this.center.add(endPrisoning);
		this.center.add(prisonerEndPrisoning);
		SpringUtilities.makeCompactGrid(center, ROWS, COLUMNS, MyUtils.getPosition(), MyUtils.getPosition(),
				MyUtils.getPosition(), MyUtils.getPosition());
		this.getContentPane().add(BorderLayout.CENTER, center);
		this.north = new PrisonManagerJPanel(new FlowLayout());
		this.north.add(prisonerID);
		this.north.add(inputPrisonerID);
		this.getContentPane().add(BorderLayout.NORTH, north);
		this.east = new PrisonManagerJPanel(new FlowLayout());
		this.textArea = new JTextArea();
		this.textArea.setEditable(false);
		this.east.add(textArea);
		this.getContentPane().add(BorderLayout.EAST, east);
		this.south = new PrisonManagerJPanel(new FlowLayout());
		this.south.add(view);
		this.south.add(back);
		this.getContentPane().add(BorderLayout.SOUTH, south);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public static ViewPrisonerView createViewPrisonerView(final int rank) {
		return new ViewPrisonerView(rank);
	}

	@Override
	public void addViewListener(ViewProfileListener viewListener) {
		view.addActionListener(viewListener);
	}

	@Override
	public void addBackListener(BackListener backListener) {
		back.addActionListener(backListener);
	}

	@Override
	public int getID() {
		if (inputPrisonerID.getText().equals(""))
			return -1;
		return Integer.valueOf(inputPrisonerID.getText());
	}

	@Override
	public void setPrisonerProfile(final String name, final String surname, final String birthday,
			final String startPrisoning, final String endPrisoning) {
		this.prisonerName.setText(name);
		this.prisonerSurname.setText(surname);
		this.prisonerBirthday.setText(birthday);
		this.prisonerStartPrisoning.setText(startPrisoning);
		this.prisonerEndPrisoning.setText(endPrisoning);
	}

	@Override
	public void displayErrorMessage(final String error) {
		JOptionPane.showMessageDialog(this, error);
	}

	@Override
	public int getRank() {
		return this.rank;
	}

	@Override
	public void setCrimesList(final List<String> crimesList) {
		textArea.setText("");
		for (final String crime : crimesList) {
			textArea.append(crime + "\n");
		}
	}
}
