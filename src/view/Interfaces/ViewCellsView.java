package view.Interfaces;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import controller.Implementations.ViewCellsControllerImpl.BackListener;
import utils.MyUtils;
import view.Components.PrisonManagerJFrame;
import view.Components.PrisonManagerJPanel;
import view.Interfaces.Inter.CommonView;
import view.Interfaces.Inter.ViewCells;

public class ViewCellsView extends PrisonManagerJFrame implements ViewCells, CommonView {

	private static final long serialVersionUID = -4382628436360052382L;
	private static final String VIEW_CELLS = "View cells";
	private static final int WIDTH = 550;
	private static final int HEIGHT = 430;
	private static final int WIDTH_TABLE = 500;
	private static final int HEIGHT_TABLE = 300;

	private int rank;
	private final PrisonManagerJPanel center;
	private final PrisonManagerJPanel south;
	private final PrisonManagerJPanel north;
	private final JButton back = new JButton(MyUtils.getBack());
	private final JLabel title = new JLabel(VIEW_CELLS);
	private JTable cellsTable = new JTable();

	private ViewCellsView(final int rank) {
		this.rank = rank;
		this.setSize(WIDTH, HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		this.north = new PrisonManagerJPanel(new FlowLayout());
		this.north.add(title);
		this.getContentPane().add(BorderLayout.NORTH, north);
		this.center = new PrisonManagerJPanel(new FlowLayout());
		this.getContentPane().add(BorderLayout.CENTER, center);
		this.south = new PrisonManagerJPanel(new FlowLayout());
		this.south.add(back);
		this.getContentPane().add(BorderLayout.SOUTH, south);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public static ViewCellsView createViewCellsView(final int rank) {
		return new ViewCellsView(rank);
	}

	@Override
	public int getRank() {
		return this.rank;
	}

	@Override
	public void addBackListener(BackListener backListener) {
		back.addActionListener(backListener);
	}

	@Override
	public void createTableCells(final JTable cellsTable) {
		this.cellsTable = cellsTable;
		this.cellsTable.setPreferredScrollableViewportSize(new Dimension(WIDTH_TABLE, HEIGHT_TABLE));
		this.cellsTable.setFillsViewportHeight(true);
		JScrollPane scrollPane = new JScrollPane(cellsTable);
		scrollPane.setVisible(true);
		this.center.add(scrollPane);
	}

	@Override
	public void displayErrorMessage(final String error) {
		JOptionPane.showMessageDialog(this, error);
	}

}
