package view.Interfaces;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import controller.Implementations.RemoveGuardControllerImpl.BackListener;
import controller.Implementations.RemoveGuardControllerImpl.RemoveGuardListener;
import utils.MyUtils;
import view.Components.PrisonManagerJFrame;
import view.Components.PrisonManagerJPanel;
import view.Interfaces.Inter.CommonView;
import view.Interfaces.Inter.RemoveGuard;

public class RemoveGuardView extends PrisonManagerJFrame implements RemoveGuard, CommonView {

	private static final long serialVersionUID = -4172744505801106816L;
	private static final String REMOVE_GUARD = "Remove guard";
	private static final String REMOVE = "Remove";
	private static final int WIDTH = 450;
	private static final int HEIGHT = 150;

	private final PrisonManagerJPanel north;
	private final JLabel title = new JLabel(REMOVE_GUARD);
	private final PrisonManagerJPanel center;
	private final JLabel guardID = new JLabel(MyUtils.getGuardId());
	private final JTextField id = new JTextField(MyUtils.getPosition());
	private final PrisonManagerJPanel south;
	private final JButton remove = new JButton(REMOVE);
	private final JButton back = new JButton(MyUtils.getBack());
	private int rank;

	private RemoveGuardView(final int rank) {
		this.rank = rank;
		this.setSize(WIDTH, HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		this.north = new PrisonManagerJPanel(new FlowLayout());
		this.north.add(title);
		this.getContentPane().add(BorderLayout.NORTH, north);
		this.center = new PrisonManagerJPanel(new FlowLayout());
		this.center.add(guardID);
		this.center.add(id);
		this.getContentPane().add(BorderLayout.CENTER, center);
		this.setVisible(true);
		this.south = new PrisonManagerJPanel(new FlowLayout());
		this.south.add(remove);
		this.south.add(back);
		this.getContentPane().add(BorderLayout.SOUTH, south);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public static RemoveGuardView createRemoveGuardView(final int rank) {
		return new RemoveGuardView(rank);
	}

	@Override
	public int getID() {
		if (id.getText().equals(""))
			return -1;
		return Integer.valueOf(id.getText());
	}

	@Override
	public void displayErrorMessage(String error) {
		JOptionPane.showMessageDialog(this, error);
	}

	@Override
	public int getRank() {
		return this.rank;
	}

	@Override
	public void addRemoveGuardListener(RemoveGuardListener removeGuardListener) {
		remove.addActionListener(removeGuardListener);
	}

	@Override
	public void addBackListener(BackListener backListener) {
		back.addActionListener(backListener);
	}
}
