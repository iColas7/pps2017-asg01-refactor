package view.Interfaces;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import controller.Implementations.AddPolicemenControllerImpl.BackListener;
import controller.Implementations.AddPolicemenControllerImpl.InsertListener;
import model.Implementations.PolicemanImpl;
import utils.MyUtils;
import view.Components.PrisonManagerJFrame;
import view.Components.PrisonManagerJPanel;
import view.Components.SpringUtilities;
import view.Interfaces.Inter.AddPoliceman;
import view.Interfaces.Inter.CommonView;

public class AddPolicemanView extends PrisonManagerJFrame implements AddPoliceman, CommonView {

	private static final long serialVersionUID = -8964073612262207713L;
	private static final int WIDTH = 450;
	private static final int HEIGHT = 400;
	private static final int ROWS = 4;
	private static final int COLUMNS = 2;
	private static final String INSERT_POLICEMAN = "Insert policeman";

	private int rank;
	private final SimpleDateFormat format = new SimpleDateFormat(MyUtils.getDataPattern());
	private final PrisonManagerJPanel south;
	private final JButton insert = new JButton(MyUtils.getInsert());
	private final PrisonManagerJPanel north;
	private final JLabel policemanName = new JLabel(MyUtils.getName());
	private final JTextField inputPolicemanName = new JTextField(MyUtils.getPosition());
	private final JLabel policemanSurname = new JLabel(MyUtils.getSurname());
	private final JTextField inputPolicemanSurname = new JTextField(MyUtils.getPosition());
	private final JLabel policemanBirthday = new JLabel(MyUtils.getBirthdayPattern());
	private final JTextField inputPolicemanBirthday = new JTextField(MyUtils.getPosition());
	private final PrisonManagerJPanel center;
	private final JButton back = new JButton(MyUtils.getBack());
	private final JLabel title = new JLabel(INSERT_POLICEMAN);
	private final JLabel prisonerID = new JLabel(MyUtils.getPrisonerId());
	private final JTextField inputPrisonerID = new JTextField(MyUtils.getPosition());

	private AddPolicemanView(final int rank) {
		this.rank = rank;
		this.setSize(WIDTH, HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		this.north = new PrisonManagerJPanel(new FlowLayout());
		this.north.add(title);
		this.getContentPane().add(BorderLayout.NORTH, north);
		this.center = new PrisonManagerJPanel(new SpringLayout());
		this.center.add(policemanName);
		this.center.add(inputPolicemanName);
		this.center.add(policemanSurname);
		this.center.add(inputPolicemanSurname);
		this.center.add(policemanBirthday);
		this.center.add(inputPolicemanBirthday);
		this.inputPolicemanBirthday.setText(MyUtils.getDefaultData());
		this.center.add(prisonerID);
		this.center.add(inputPrisonerID);
		this.inputPrisonerID.setText("0");
		SpringUtilities.makeCompactGrid(center, ROWS, COLUMNS, MyUtils.getPosition(), MyUtils.getPosition(),
				MyUtils.getPosition(), MyUtils.getPosition());
		this.getContentPane().add(BorderLayout.CENTER, center);
		this.south = new PrisonManagerJPanel(new FlowLayout());
		this.south.add(insert);
		this.south.add(back);
		this.getContentPane().add(BorderLayout.SOUTH, south);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public static AddPolicemanView createAddPolicemanView(final int rank) {
		return new AddPolicemanView(rank);
	}

	@Override
	public PolicemanImpl getPoliceman() {
		Date policemanBirthday = new Date();
		try {
			policemanBirthday = format.parse(inputPolicemanBirthday.getText());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		PolicemanImpl policeman = PolicemanImpl.createPolicemanImpl(inputPolicemanName.getText(),
				inputPolicemanSurname.getText(), policemanBirthday, Integer.valueOf(inputPrisonerID.getText()));
		return policeman;
	}

	@Override
	public int getRank() {
		return this.rank;
	}

	@Override
	public void displayErrorMessage(final String error) {
		JOptionPane.showMessageDialog(this, error);
	}

	@Override
	public void addBackListener(final BackListener backListener) {
		back.addActionListener(backListener);

	}

	@Override
	public void addInsertPolicemanListener(final InsertListener insertListener) {
		insert.addActionListener(insertListener);

	}

}
