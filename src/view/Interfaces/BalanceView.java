package view.Interfaces;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import controller.Implementations.BalanceControllerImpl.BackListener;
import utils.MyUtils;
import view.Components.PrisonManagerJFrame;
import view.Components.PrisonManagerJPanel;
import view.Interfaces.Inter.Balance;
import view.Interfaces.Inter.CommonView;

public class BalanceView extends PrisonManagerJFrame implements Balance, CommonView {

	private static final long serialVersionUID = -9027369697644712989L;
	private static final int WIDTH = 600;
	private static final int HEIGHT = 400;
	private static final int WIDTH_SCROLL = 550;
	private static final int HEIGHT_SCROLL = 260;

	private final PrisonManagerJPanel center;
	private final PrisonManagerJPanel south;
	private final PrisonManagerJPanel north;
	private final JButton back = new JButton(MyUtils.getBack());
	private final JLabel balance = new JLabel(MyUtils.getBalance());
	private JTable balanceTable = new JTable();
	private final int guardRank;

	private BalanceView(final int rank) {
		this.guardRank = rank;
		this.setSize(WIDTH, HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		this.north = new PrisonManagerJPanel(new FlowLayout());
		this.north.add(balance);
		this.getContentPane().add(BorderLayout.NORTH, north);
		this.center = new PrisonManagerJPanel(new FlowLayout());
		this.getContentPane().add(BorderLayout.CENTER, center);
		this.south = new PrisonManagerJPanel(new FlowLayout());
		this.south.add(back);
		this.getContentPane().add(BorderLayout.SOUTH, south);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public static BalanceView createBalanceView(final int rank) {
		return new BalanceView(rank);
	}

	@Override
	public int getRank() {
		return this.guardRank;
	}

	@Override
	public void addBackListener(final BackListener backListener) {
		back.addActionListener(backListener);
	}

	@Override
	public void setBalanceLabel(final String balanceLabel) {
		balance.setText(MyUtils.getBalance() + balanceLabel);
	}

	@Override
	public void createBalanceTable(final JTable balanceTable) {
		this.setBalanceTable(balanceTable);
		balanceTable.setPreferredScrollableViewportSize(new Dimension(WIDTH_SCROLL, HEIGHT_SCROLL));
		JScrollPane scrollPaneBalanceTable = new JScrollPane(balanceTable);
		scrollPaneBalanceTable.setVisible(true);
		center.add(scrollPaneBalanceTable);
	}

	@Override	
	public JTable getBalanceTable() {
		return balanceTable;
	}

	@Override
	public void setBalanceTable(final JTable balanceTable) {
		this.balanceTable = balanceTable;
	}

	@Override
	public void displayErrorMessage(String error){
		JOptionPane.showMessageDialog(this, error);
	}
}
