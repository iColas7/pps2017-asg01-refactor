package view.Interfaces;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import controller.Implementations.AddVisitorsControllerImpl.InsertListener;
import controller.Implementations.AddVisitorsControllerImpl.BackListener;
import model.Implementations.VisitorImpl;
import utils.MyUtils;
import view.Components.PrisonManagerJFrame;
import view.Components.PrisonManagerJPanel;
import view.Components.SpringUtilities;
import view.Interfaces.Inter.AddVisitors;
import view.Interfaces.Inter.CommonView;

public class AddVisitorsView extends PrisonManagerJFrame implements AddVisitors, CommonView {

	private static final long serialVersionUID = -8964073612262207713L;
	private static final int WIDTH = 450;
	private static final int HEIGHT = 400;
	private static final int ROWS = 4;
	private static final int COLUMNS = 2;

	private int rank;
	private final SimpleDateFormat format = new SimpleDateFormat(MyUtils.getDataPattern());
	private final PrisonManagerJPanel south;
	private final JButton insert = new JButton(MyUtils.getInsert());
	private final PrisonManagerJPanel north;
	private final JLabel visitorName = new JLabel(MyUtils.getName());
	private final JTextField inputVisitorName = new JTextField(MyUtils.getPosition());
	private final JLabel visitorSurname = new JLabel(MyUtils.getSurname());
	private final JTextField inputVisitorSurname = new JTextField(MyUtils.getPosition());
	private final JLabel visitorBirthday = new JLabel(MyUtils.getBirthdayPattern());
	private final JTextField inputVisitorBirthday = new JTextField(MyUtils.getPosition());
	private final PrisonManagerJPanel center;
	private final JButton back = new JButton(MyUtils.getBack());
	private final JLabel title = new JLabel(MyUtils.getVisitorInserted());
	private final JLabel prisonerID = new JLabel(MyUtils.getPrisonerId());
	private final JTextField inputPrisonerID = new JTextField(MyUtils.getPosition());

	private AddVisitorsView(final int rank) {
		this.rank = rank;
		this.setSize(WIDTH, HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		this.north = new PrisonManagerJPanel(new FlowLayout());
		this.north.add(title);
		this.getContentPane().add(BorderLayout.NORTH, north);
		this.center = new PrisonManagerJPanel(new SpringLayout());
		this.center.add(visitorName);
		this.center.add(inputVisitorName);
		this.center.add(visitorSurname);
		this.center.add(inputVisitorSurname);
		this.center.add(visitorBirthday);
		this.center.add(inputVisitorBirthday);
		this.inputVisitorBirthday.setText(MyUtils.getDefaultData());
		this.center.add(prisonerID);
		this.center.add(inputPrisonerID);
		this.inputPrisonerID.setText("0");
		SpringUtilities.makeCompactGrid(center, ROWS, COLUMNS, MyUtils.getPosition(), MyUtils.getPosition(),
				MyUtils.getPosition(), MyUtils.getPosition());
		this.getContentPane().add(BorderLayout.CENTER, center);
		this.south = new PrisonManagerJPanel(new FlowLayout());
		this.south.add(insert);
		this.south.add(back);
		this.getContentPane().add(BorderLayout.SOUTH, south);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public static AddVisitorsView createAddVisitorsView(final int rank) {
		return new AddVisitorsView(rank);
	}

	@Override
	public VisitorImpl getVisitor() {
		Date visitorBirthday = null;
		try {
			visitorBirthday = format.parse(inputVisitorBirthday.getText());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		VisitorImpl visitor = VisitorImpl.createVisitorImpl(inputVisitorName.getText(), inputVisitorSurname.getText(),
				visitorBirthday, Integer.valueOf(inputPrisonerID.getText()));
		return visitor;
	}

	@Override
	public int getRank() {
		return this.rank;
	}

	@Override
	public void displayErrorMessage(String error) {
		JOptionPane.showMessageDialog(this, error);
	}

	@Override
	public void addBackListener(BackListener backListener) {
		back.addActionListener(backListener);
	}

	@Override
	public void addInsertVisitorListener(InsertListener insertListener) {
		insert.addActionListener(insertListener);
	}

}
