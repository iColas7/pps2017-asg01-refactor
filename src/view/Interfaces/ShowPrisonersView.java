package view.Interfaces;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import controller.Implementations.ShowPrisonersControllerImpl.BackListener;
import controller.Implementations.ShowPrisonersControllerImpl.PrisonersInPrison;
import utils.MyUtils;
import view.Components.PrisonManagerJFrame;
import view.Components.PrisonManagerJPanel;
import view.Interfaces.Inter.CommonView;
import view.Interfaces.Inter.ShowPrisoners;

public class ShowPrisonersView extends PrisonManagerJFrame implements ShowPrisoners, CommonView {

	private static final long serialVersionUID = 7096756054745433188L;
	private static final String COMPUTE = "Compute";
	private static final String START_PRISONING = "Prisoning from (mm/dd/yyyy)";
	private static final String END_PRISONING = "Prisoning to (mm/dd/yyyy)";
	private static final int WIDTH = 800;
	private static final int HEIGHT = 370;
	private static final int WIDTH_TABLE = 700;
	private static final int HEIGHT_TABLE = 200;
	private static final String START_PRISONING_EXAMPLE = "01/31/1980";
	private static final String END_PRISONING_EXAMPLE = "01/31/2021";
	
	private final PrisonManagerJPanel north;
	private final JLabel startPrisoning = new JLabel(START_PRISONING);
	private final JLabel endPrisoning = new JLabel(END_PRISONING);
	private final JTextField inputStartPrisoning = new JTextField(MyUtils.getPosition());
	private final JTextField inputEndPrisoning = new JTextField(MyUtils.getPosition());
	private final PrisonManagerJPanel center;
	private JTable prisonersTable = new JTable();
	private final PrisonManagerJPanel south;
	private final JButton compute = new JButton(COMPUTE);
	private final JButton back = new JButton(MyUtils.getBack());
	private JScrollPane scrollPane;
	private int rank;
	private SimpleDateFormat format = new SimpleDateFormat(MyUtils.getDataPattern());

	private ShowPrisonersView(final int rank) {
		this.rank = rank;
		this.setSize(WIDTH, HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		this.north = new PrisonManagerJPanel(new FlowLayout());
		this.north.add(startPrisoning);
		this.north.add(inputStartPrisoning);
		this.inputStartPrisoning.setText(START_PRISONING_EXAMPLE);
		this.north.add(endPrisoning);
		this.north.add(inputEndPrisoning);
		this.inputEndPrisoning.setText(END_PRISONING_EXAMPLE);
		this.getContentPane().add(BorderLayout.NORTH, north);
		this.center = new PrisonManagerJPanel(new FlowLayout());
		this.scrollPane = new JScrollPane(prisonersTable);
		this.getContentPane().add(BorderLayout.CENTER, center);
		this.south = new PrisonManagerJPanel(new FlowLayout());
		this.south.add(compute);
		this.south.add(back);
		this.getContentPane().add(BorderLayout.SOUTH, south);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public static ShowPrisonersView createShowPrisonersView(final int rank) {
		return new ShowPrisonersView(rank);
	}

	@Override
	public void createPrisonersTable(final JTable prisonersTable) {
		this.center.removeAll();
		this.prisonersTable = prisonersTable;
		this.prisonersTable.setPreferredScrollableViewportSize(new Dimension(WIDTH_TABLE, HEIGHT_TABLE));
		this.scrollPane = new JScrollPane(prisonersTable);
		this.center.add(scrollPane);
		this.scrollPane.revalidate();
		this.setVisible(true);
	}

	@Override
	public int getRank() {
		return rank;
	}

	@Override
	public void addBackListener(BackListener backListener) {
		back.addActionListener(backListener);
	}

	@Override
	public void addComputeListener(PrisonersInPrison computeListener) {
		compute.addActionListener(computeListener);
	}

	@Override
	public Date getFromDate() {
		Date startPrisoning = null;
		try {
			startPrisoning = format.parse(inputStartPrisoning.getText());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return startPrisoning;
	}

	@Override
	public Date getToDate() {
		Date endPrisoning = null;
		try {
			endPrisoning = format.parse(inputEndPrisoning.getText());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return endPrisoning;
	}

	@Override
	public void displayErrorMessage(String error) {
		JOptionPane.showMessageDialog(this, error);
	}
}
