package view.Interfaces;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import controller.Implementations.InsertGuardControllerImpl.BackListener;
import controller.Implementations.InsertGuardControllerImpl.InsertListener;
import model.Interfaces.Guard;
import utils.MyUtils;
import view.Components.PrisonManagerJFrame;
import view.Components.PrisonManagerJPanel;
import view.Components.SpringUtilities;
import view.Interfaces.Inter.CommonView;
import view.Interfaces.Inter.InsertGuard;

public class InsertGuardView extends PrisonManagerJFrame implements InsertGuard, CommonView {

	private static final long serialVersionUID = 6919464397187101572L;

	private static final String RANK = "Rank (1-2-3)";
	private static final int FIELD_POSITION = 8;
	private static final String SET_PASSWORD = "Password (min. 6 chars)";
	private static final int WIDTH = 450;
	private static final int HEIGHT = 400;
	private static final int ROWS = 7;
	private static final int COLUMNS = 2;

	private final PrisonManagerJPanel south;
	private final JButton insert = new JButton(MyUtils.getInsert());
	private final PrisonManagerJPanel north;
	private final JLabel guardID = new JLabel(MyUtils.getGuardId());
	private final JTextField inputGuardID = new JTextField(MyUtils.getPosition());
	private final JLabel guardName = new JLabel(MyUtils.getName());
	private final JTextField inputGuardName = new JTextField(MyUtils.getPosition());
	private final JLabel guardSurname = new JLabel(MyUtils.getSurname());
	private final JTextField inputGuardSurname = new JTextField(MyUtils.getPosition());
	private final JLabel guardBirthday = new JLabel(MyUtils.getBirthdayPattern());
	private final JTextField inputGuardBirthday = new JTextField(MyUtils.getPosition());
	private final PrisonManagerJPanel center;
	private final JLabel guardRank = new JLabel(RANK);
	private final JTextField inputGuardRank = new JTextField(FIELD_POSITION);
	private final JLabel guardTelephoneNumber = new JLabel(MyUtils.getTelephoneNumber());
	private final JTextField inputGuardTelephoneNumber = new JTextField(FIELD_POSITION);
	private final JLabel guardPassword = new JLabel(SET_PASSWORD);
	private final JTextField inputGuardPassword = new JTextField(FIELD_POSITION);
	private final JButton back = new JButton(MyUtils.getBack());
	private final JLabel title = new JLabel(MyUtils.getGuardInserted());
	private SimpleDateFormat format = new SimpleDateFormat(MyUtils.getDataPattern());
	private int rank;

	private InsertGuardView(final int rank) {
		this.rank = rank;
		this.setSize(WIDTH, HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		this.north = new PrisonManagerJPanel(new FlowLayout());
		this.north.add(title);
		this.getContentPane().add(BorderLayout.NORTH, north);
		this.center = new PrisonManagerJPanel(new SpringLayout());
		this.center.add(guardID);
		this.center.add(inputGuardID);
		this.inputGuardID.setText("0");
		this.center.add(guardName);
		this.center.add(inputGuardName);
		this.center.add(guardSurname);
		this.center.add(inputGuardSurname);
		this.center.add(guardBirthday);
		this.center.add(inputGuardBirthday);
		this.inputGuardBirthday.setText(MyUtils.getDefaultData());
		this.center.add(guardTelephoneNumber);
		this.center.add(inputGuardTelephoneNumber);
		this.center.add(guardRank);
		this.center.add(inputGuardRank);
		this.inputGuardRank.setText("0");
		this.center.add(guardPassword);
		this.center.add(inputGuardPassword);
		SpringUtilities.makeCompactGrid(center, ROWS, COLUMNS, MyUtils.getPosition(), MyUtils.getPosition(),
				MyUtils.getPosition(), MyUtils.getPosition());
		this.getContentPane().add(BorderLayout.CENTER, center);
		this.south = new PrisonManagerJPanel(new FlowLayout());
		this.south.add(insert);
		this.south.add(back);
		this.getContentPane().add(BorderLayout.SOUTH, south);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public static InsertGuardView createInsertGuardView(final int rank) {
		return new InsertGuardView(rank);
	}

	public void addBackListener(final BackListener backListener) {
		back.addActionListener(backListener);
	}

	@Override
	public void displayErrorMessage(final String error) {
		JOptionPane.showMessageDialog(this, error);
	}

	public void addInsertListener(final InsertListener insertListener) {
		insert.addActionListener(insertListener);
	}

	@Override
	public int getRank() {
		return this.rank;
	}

	@Override
	public Guard getGuard() {
		Date birthday = null;
		try {
			birthday = format.parse(inputGuardBirthday.getText());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Guard guard = new model.Implementations.GuardImplBuilder().setName(inputGuardName.getText())
				.setSurname(inputGuardSurname.getText()).setBirthdate(birthday)
				.setRank(Integer.valueOf(inputGuardRank.getText()))
				.setTelephoneNumber(inputGuardTelephoneNumber.getText())
				.setGuardId(Integer.valueOf(inputGuardID.getText())).setPassword(inputGuardPassword.getText())
				.createGuardImpl();
		return guard;
	}
}
