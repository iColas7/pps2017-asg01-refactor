package view.Interfaces.Inter;

import controller.Implementations.MainControllerImpl.InsertPrisonerListener;
import controller.Implementations.MainControllerImpl.LogoutListener;
import controller.Implementations.MainControllerImpl.MoreFunctionsListener;
import controller.Implementations.MainControllerImpl.RemovePrisonerListener;
import controller.Implementations.MainControllerImpl.SupervisorListener;
import controller.Implementations.MainControllerImpl.ViewPrisonerListener;

public interface Main {

	/**
	 * @param logoutListener
	 */
	void addLogoutListener(final LogoutListener logoutListener);

	/**
	 * @param insertPrisonerListener
	 */
	void addInsertPrisonerListener(final InsertPrisonerListener insertPrisonerListener);

	/**
	 * @param removePrisonerListener
	 */
	void addRemovePrisonerListener(final RemovePrisonerListener removePrisonerListener);

	/**
	 * @param viewPrisonerListener
	 */
	void addViewPrisonerListener(final ViewPrisonerListener viewPrisonerListener);

	/**
	 * @param moreFListener
	 */
	void addMoreFunctionsListener(final MoreFunctionsListener moreFListener);

	/**
	 * @param supervisorListener
	 */
	void addSupervisorListener(final SupervisorListener supervisorListener);
}
