package view.Interfaces.Inter;

import java.util.Date;
import java.util.List;

import controller.Implementations.InsertPrisonerControllerImpl.AddCrimeListener;
import controller.Implementations.InsertPrisonerControllerImpl.BackListener;
import controller.Implementations.InsertPrisonerControllerImpl.InsertPrisonerListener;

public interface InsertPrisoner {

	/**
	 * @param addPrisonerListener
	 */
	void addInsertPrisonerListener(final InsertPrisonerListener addPrisonerListener);

	/**
	 * @param backListener
	 */
	void addBackListener(final BackListener backListener);

	/**
	 * @return Prisoner's ID
	 */
	int getPrisonerID();

	/**
	 * @return Prisoner's name
	 */
	String getPrisonerName();

	/**
	 * @return Prisoner's surname
	 */
	String getPrisonerSurname();

	/**
	 * @return Start prisoning day
	 */
	Date getStartPrisoning();

	/**
	 * @return End prisoning day
	 */
	Date getEndPrisoning();

	/**
	 * @return Prisoner's birthday
	 */
	Date getPrisonerBirthday();

	/**
	 * @return Prisoner's cell ID
	 */
	int getCellID();

	/**
	 * @param crimesList
	 *            Crimes list
	 */
	void setCrimesList(final List<String> crimesList);

	/**
	 * @return Crimes list
	 */
	List<String> getCrimesList();

	/**
	 * @return Crime in combobox
	 */
	String getCrimeSelected();

	/**
	 * @param addCrimeListener
	 */
	void addAddCrimeListener(final AddCrimeListener addCrimeListener);
}
