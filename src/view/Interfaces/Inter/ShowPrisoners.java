package view.Interfaces.Inter;

import java.util.Date;

import javax.swing.JTable;

import controller.Implementations.ShowPrisonersControllerImpl.BackListener;
import controller.Implementations.ShowPrisonersControllerImpl.PrisonersInPrison;

public interface ShowPrisoners {

	/**
	 * @param prisonersTable Prisoners' table
	 */
	 void createPrisonersTable(final JTable prisonersTable);
	
	/**
	 * @param backListener
	 */
	 void addBackListener(final BackListener backListener);
	
	/**
	 * @param computeListener
	 */
	 void addComputeListener(final PrisonersInPrison computeListener);
	
	/**
	 * @return Start day to research
	 */
	 Date getFromDate();

	/**
	 * @return End date of research
	 */
	 Date getToDate();
}
