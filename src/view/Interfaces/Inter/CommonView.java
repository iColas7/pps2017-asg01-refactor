package view.Interfaces.Inter;

public abstract interface CommonView {
	
	/**
	 * @return Guard's rank
	 */
	int getRank();
	
	/**
	 * Display an error message
	 * 
	 * @param error
	 *            the message
	 */
	void displayErrorMessage(final String error);

}
