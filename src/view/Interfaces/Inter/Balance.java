package view.Interfaces.Inter;

import javax.swing.JTable;

import controller.Implementations.BalanceControllerImpl.BackListener;

public interface Balance {

	/**
	 * @param balanceTable
	 */
	void createBalanceTable(final JTable balanceTable);

	/**
	 * @param balanceLabel
	 */
	void setBalanceLabel(final String balanceLabel);

	/**
	 * @param backListener
	 */
	void addBackListener(final BackListener backListener);

	/**
	 * 
	 * @return Balance table
	 */
	JTable getBalanceTable();

	/**
	 * 
	 * @param balanceTable
	 */
	void setBalanceTable(final JTable balanceTable);
}
