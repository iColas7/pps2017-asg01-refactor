package view.Interfaces.Inter;

import java.util.List;

import controller.Implementations.ViewPrisonerControllerImpl.BackListener;
import controller.Implementations.ViewPrisonerControllerImpl.ViewProfileListener;

public interface ViewPrisoner {

	/**
	 * @param viewListener
	 */
	void addViewListener(final ViewProfileListener viewListener);

	/**
	 * @param backListener
	 */
	void addBackListener(final BackListener backListener);

	/**
	 * @return Prisoner's ID
	 */
	public int getID();

	/**
	 * @param name
	 *            Prisoner's name
	 * @param surname
	 *            Prisoner's surname
	 * @param birthday
	 *            Prisoner's birthday
	 * @param startPrisoning
	 *            Prisoner's start prisoning
	 * @param endPrisoning
	 *            Prisoner's end prisoning
	 */
	void setPrisonerProfile(final String name, final String surname, final String birthday, final String startPrisoning,
			final String endPrisoning);

	/**
	 * @param crimesList
	 *            lista di crimini
	 */
	void setCrimesList(final List<String> crimesList);

}
