package view.Interfaces.Inter;

import controller.Implementations.AddPolicemenControllerImpl.BackListener;
import controller.Implementations.AddPolicemenControllerImpl.InsertListener;
import model.Implementations.PolicemanImpl;

public interface AddPoliceman {

	/**
	 * @return Policeman
	 */
	PolicemanImpl getPoliceman();

	void addBackListener(final BackListener backListener);

	void addInsertPolicemanListener(final InsertListener insertListener);
}
