package view.Interfaces.Inter;

import controller.Implementations.ViewGuardControllerImpl.BackListener;
import controller.Implementations.ViewGuardControllerImpl.ViewGuardListener;

public interface ViewGuard {

	/**
	 * @param viewListener
	 */
	void addViewListener(final ViewGuardListener viewListener);

	/**
	 * @param backListener
	 */
	void addBackListener(final BackListener backListener);

	/**
	 * @return Guard's ID
	 */
	int getID();

	/**
	 * @param name
	 *            Guard's name
	 */
	void setName(final String name);

	/**
	 * @param surname
	 *            Guard's surname
	 */
	void setSurname(final String surname);

	/**
	 * @param birthday
	 *            Guard's birthday
	 */
	void setBirth(final String birthday);

	/**
	 * @param rank
	 *            Guard's rank
	 */
	void setRank(final String rank);

	/**
	 * @param telephone
	 *            Guard's telephone number
	 */
	void setTelephone(final String telephone);
}
