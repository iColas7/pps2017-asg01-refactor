package view.Interfaces.Inter;

import controller.Implementations.SupervisorControllerImpl.BackListener;
import controller.Implementations.SupervisorControllerImpl.InsertGuardListener;
import controller.Implementations.SupervisorControllerImpl.RemoveGuardListener;
import controller.Implementations.SupervisorControllerImpl.ShowPrisonersListener;
import controller.Implementations.SupervisorControllerImpl.ViewGuardListener;

public interface SupervisorFunctions {

	/**
	 * @param backListener
	 */
	void addBackListener(final BackListener backListener);

	/**
	 * @param showPrisonersListeners
	 */
	void addShowPrisonersListener(final ShowPrisonersListener showPrisonersListeners);

	/**
	 * @param insertGaurdListener
	 */
	void addInsertGuardListener(final InsertGuardListener insertGaurdListener);

	/**
	 * @param removeListener
	 */
	void addRemoveGuardListener(final RemoveGuardListener removeListener);

	/**
	 * @param viewListener
	 */
	void addviewGuardListener(final ViewGuardListener viewListener);
}
