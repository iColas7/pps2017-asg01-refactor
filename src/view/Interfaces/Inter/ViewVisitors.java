package view.Interfaces.Inter;

import javax.swing.JTable;

import controller.Implementations.ViewVisitorsControllerImpl.BackListener;

public interface ViewVisitors {

	/**
	 * @param backListener
	 */
	void addBackListener(final BackListener backListener);

	/**
	 * @param visitorsTable
	 */
	void createVisitorsTable(final JTable visitorsTable);
}
