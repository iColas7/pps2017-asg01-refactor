package view.Interfaces.Inter;

import controller.Implementations.InsertGuardControllerImpl.BackListener;
import controller.Implementations.InsertGuardControllerImpl.InsertListener;
import model.Interfaces.Guard;

public interface InsertGuard {

	/**
	 * @return Guard
	 */
	Guard getGuard();

	/**
	 * @param insertListener
	 */
	void addInsertListener(final InsertListener insertListener);

	/**
	 * @param backListener
	 */
	void addBackListener(final BackListener backListener);
}
