package view.Interfaces.Inter;

import javax.swing.JTable;

import controller.Implementations.ViewCellsControllerImpl.BackListener;

public interface ViewCells {

	/**
	 * @param backListener
	 */
	void addBackListener(final BackListener backListener);

	/**
	 * @param tableCells
	 */
	void createTableCells(final JTable tableCells);

}
