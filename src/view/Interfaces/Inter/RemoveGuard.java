package view.Interfaces.Inter;

import controller.Implementations.RemoveGuardControllerImpl.BackListener;
import controller.Implementations.RemoveGuardControllerImpl.RemoveGuardListener;

public interface RemoveGuard {

	/**
	 * @return Guard's ID
	 */
	int getID();

	/**
	 * @param removeGuardListener
	 */
	void addRemoveGuardListener(final RemoveGuardListener removeGuardListener);

	/**
	 * @param backListener
	 */
	void addBackListener(final BackListener backListener);
}
