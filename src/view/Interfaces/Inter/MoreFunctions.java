package view.Interfaces.Inter;

import controller.Implementations.MoreFunctionsControllerImpl.*;

public interface MoreFunctions {

	/**
	 * @param backListener
	 */
	void addBackListener(final BackListener backListener);

	/**
	 * @param addMovementListener
	 */
	void addAddMovementListener(final AddMovementListener addMovementListener);

	/**
	 * @param balanceListener
	 */
	void addBalanceListener(final BalanceListener balanceListener);

	/**
	 * @param chartPrisonersForYear
	 */
	void addChartPrisonersForYear(final OpenChartPrisonersForYear chartPrisonersForYear);

	/**
	 * 
	 * @param chartCrimesPercent
	 */
	void addChartCrimesPercent(final OpenChartPercentCrimes chartCrimesPercent);

	/**
	 * @param addVisitorsListener
	 */
	void addAddVisitorsListener(final AddVisitorsListener addVisitorsListener);

	/**
	 * @param viewVisitorsListener
	 */
	void addViewVisitorsListener(final ViewVisitorsListener viewVisitorsListener);
	
	/**
	 * @param addPolicemenListener
	 */
	void addAddPolicemanListener(final AddPolicemanListener addVisitorsListener);

	/**
	 * @param viewPolicemenListener
	 */
	void addViewPolicemanListener(final ViewPolicemanListener viewVisitorsListener);

	/**
	 * @param viewCellsListener
	 */
	void addViewCellsListener(final ViewCellsListener viewCellsListener);
}
