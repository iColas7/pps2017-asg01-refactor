package view.Interfaces.Inter;

import javax.swing.JTable;

import controller.Implementations.ViewPolicemenControllerImpl.BackListener;

public interface ViewPolicemen {

	/**
	 * @param backListener
	 */
	void addBackListener(final BackListener backListener);

	/**
	 * @param policemenTable
	 */
	void createPolicemenTable(final JTable visitorsTable);

}
