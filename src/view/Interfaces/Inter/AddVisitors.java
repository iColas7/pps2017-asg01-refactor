package view.Interfaces.Inter;

import controller.Implementations.AddVisitorsControllerImpl.BackListener;
import controller.Implementations.AddVisitorsControllerImpl.InsertListener;
import model.Implementations.VisitorImpl;

public interface AddVisitors {

	/**
	 * @return Visitor
	 */
	VisitorImpl getVisitor();
	
	/**
	 * 
	 * @param backListener
	 */
	void addBackListener(final BackListener backListener);

	/**
	 * 
	 * @param insertListener
	 */
	void addInsertVisitorListener(final InsertListener insertListener);
}
