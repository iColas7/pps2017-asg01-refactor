package view.Interfaces.Inter;

import controller.Implementations.LoginControllerImpl.LoginListener;

public interface Login {

	/**
	 * @return Username
	 */
	String getUsername();
	
	/**
	 * Set empty username (in error's case)
	 */
	void setUsername();

	/**
	 * @return Password
	 */
	String getPassword();
	
	/**
	 * Set guard's password
	 */
	void setPassword();

	/**
	 * @param loginListener
	 */
	void addLoginListener(LoginListener loginListener);
}
