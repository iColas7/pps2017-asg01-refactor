package view.Interfaces.Inter;

import controller.Implementations.AddMovementControllerImpl.BackListener;
import controller.Implementations.AddMovementControllerImpl.InsertListener;

public interface AddMovement {

	/**
	 * @return Movement's description
	 */
	String getMovementDescription();

	/**
	 * @return Movement's value
	 */
	Double getMovementValue();

	/**
	 * @return Movement's symbol
	 */
	String getMovementSymbol();
	
	/**
	 * 
	 * @param backListener
	 */
	void addBackListener(final BackListener backListener);

	/**
	 * 
	 * @param insertListener
	 */
	void addInsertListener(final InsertListener insertListener);

}
