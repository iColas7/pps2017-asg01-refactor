package view.Interfaces.Inter;

import controller.Implementations.RemovePrisonerControllerImpl.BackListener;
import controller.Implementations.RemovePrisonerControllerImpl.RemoveListener;

public interface RemovePrisoner {

	/**
	 * @param removeListener
	 */
	 void addRemoveListener(final RemoveListener removeListener);

	/**
	 * @param backListener
	 */
	 void addBackListener(final BackListener backListener);

	/**
	 * @return Prisoner's ID
	 */
	 int getID();
}
