package controller.Interfaces;

import java.io.IOException;

import model.Interfaces.Prisoner;

public interface InsertPrisonerController {

	/**
	 * Verify that name and surname of prisoner aren't empty
	 * 
	 * @param prisoner
	 *            Prisoner to check
	 * @return true If a field is empty
	 */
	boolean isSomethingEmpty(final Prisoner prisoner);

	/**
	 * Insert prisoner
	 * 
	 * @throws IOException
	 */
	void insertPrisoner() throws IOException;
}
