package controller.Interfaces;

import java.io.IOException;

public interface RemoveGuardController {

	/**
	 * Remove guard from guards' list
	 * @throws IOException 
	 */
	void removeGuard() throws IOException;
}
