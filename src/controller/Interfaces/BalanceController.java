package controller.Interfaces;

import java.io.IOException;

public interface BalanceController {

	/**
	 * Method to show balance with table as a graph - First calculate balance -
	 * Second show all movements with description, amount and date
	 * 
	 * @throws IOException
	 * @throws Exception
	 */
	void showBalance() throws IOException, Exception;
}
