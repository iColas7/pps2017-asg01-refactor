package controller.Interfaces;

import javax.swing.JTable;

public interface ViewPolicemenController {

	/**
	 * @return policemen' table
	 */
	JTable getPolicemanTable();
}
