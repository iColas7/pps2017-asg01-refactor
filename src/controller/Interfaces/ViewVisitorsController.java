package controller.Interfaces;

import javax.swing.JTable;

public interface ViewVisitorsController {

	/**
	 * @return visitors' table
	 */
	JTable getVisitorsTable();
}
