package controller.Interfaces;

import java.io.IOException;

public interface RemovePrisonerController {

	/**
	 * Remove prisoner from prisoners' list
	 * 
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	void removePrisoner() throws IOException, ClassNotFoundException;
}
