package controller.Interfaces;

import java.util.List;
import model.Interfaces.Prisoner;

public interface MoreFunctionsController {

	/**
	 * Create chart of prisoners for year
	 */
	void createChartPrisonersForYear();

	/**
	 * data una lista di prigionieri ritorna l'anno maggiore tra le date di fine
	 * prigionia
	 * 
	 * @param prisonersList
	 *            Prisoners' list
	 * @return Max year of end prisoning
	 */
	int getMaxYearEndPrisoning(final List<Prisoner> prisonersList);

	/**
	 * Create chart of crimes percent
	 */
	void createChartCrimesPercent();
}
