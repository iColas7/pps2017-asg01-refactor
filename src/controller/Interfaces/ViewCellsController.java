package controller.Interfaces;

import java.io.IOException;

import javax.swing.JTable;

public interface ViewCellsController {

	/**
	 * @return Table with all elements of any prisoners
	 * @throws IOException
	 */
	JTable getCellsTable() throws IOException;
}
