package controller.Interfaces;

import java.io.IOException;

import model.Interfaces.Guard;

public interface InsertGuardController {

	/**
	 * Insert new guard
	 * 
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	void insertGuard() throws ClassNotFoundException, IOException;

	/**
	 * Controls if guard is implemented correctly
	 * 
	 * @param guard
	 *            The guard
	 * @return true If guard is badly implemented
	 */
	boolean isSomethingEmpty(final Guard guard);
}
