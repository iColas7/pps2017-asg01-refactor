package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JTable;

import model.Interfaces.Prisoner;
import utils.MyUtils;
import view.Components.PrisonManagerJFrame;
import view.Interfaces.ShowPrisonersView;
import view.Interfaces.SupervisorFunctionsView;

public class ShowPrisonersControllerImpl extends PrisonManagerJFrame {

	private static final long serialVersionUID = -2056633481557914162L;
	private static ShowPrisonersView showPrisonersView;

	private ShowPrisonersControllerImpl(final ShowPrisonersView showPrisonersView) {
		ShowPrisonersControllerImpl.showPrisonersView = showPrisonersView;
		ShowPrisonersControllerImpl.showPrisonersView.addBackListener(new BackListener());
		ShowPrisonersControllerImpl.showPrisonersView.addComputeListener(new PrisonersInPrison());
	}

	public static ShowPrisonersControllerImpl createShowPrisonersControllerImpl(
			final ShowPrisonersView showPrisonersView) {
		return new ShowPrisonersControllerImpl(showPrisonersView);
	}

	/**
	 * Listener to back previous view
	 */
	public static class BackListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent arg0) {
			showPrisonersView.dispose();
			SupervisorControllerImpl.createSupervisorControllerImpl(
					SupervisorFunctionsView.createSupervisorFunctionsView(showPrisonersView.getRank()));
		}
	}

	/**
	 * crea una tabella contenente i prigionieri che tra le due date prese in input
	 * erano in prigione
	 */
	public static class PrisonersInPrison implements ActionListener {

		@Override
		public void actionPerformed(final ActionEvent arg0) {
			Date startPrisoning = showPrisonersView.getFromDate();
			Date endPrisoning = showPrisonersView.getToDate();
			List<Prisoner> prisonersList = new ArrayList<>();
			List<Prisoner> prisonersInThisPeriodInPrison = new ArrayList<>();

			try {
				prisonersList = MainControllerImpl.getPrisoners();
			} catch (final ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}

			for (final Prisoner prisoner : prisonersList) {
				if (prisoner.getStartPrisoning().before(endPrisoning)
						&& prisoner.getEndPrisoning().after(startPrisoning)) {
					prisonersInThisPeriodInPrison.add(prisoner);
				}
			}

			JTable prisonersInPrisonTable = new JTable();
			String[] prisonersVector = { MyUtils.getPrisonerId(), MyUtils.getName(), MyUtils.getSurname(),
					MyUtils.getBirthday(), MyUtils.getStartPrisoning(), MyUtils.getEndPrisoning() };
			String[][] prisonersMatrix = new String[prisonersInThisPeriodInPrison.size()][prisonersVector.length];
			for (int i = 0; i < prisonersInThisPeriodInPrison.size(); i++) {
				prisonersMatrix[i][0] = String.valueOf(prisonersInThisPeriodInPrison.get(i).getPrisonerID());
				prisonersMatrix[i][1] = prisonersInThisPeriodInPrison.get(i).getName();
				prisonersMatrix[i][2] = prisonersInThisPeriodInPrison.get(i).getSurname();
				prisonersMatrix[i][3] = prisonersInThisPeriodInPrison.get(i).getBirthdate().toString();
				prisonersMatrix[i][4] = prisonersInThisPeriodInPrison.get(i).getStartPrisoning().toString();
				prisonersMatrix[i][5] = prisonersInThisPeriodInPrison.get(i).getEndPrisoning().toString();
			}
			prisonersInPrisonTable = new JTable(prisonersMatrix, prisonersVector);
			showPrisonersView.createPrisonersTable(prisonersInPrisonTable);
		}
	}
}
