package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.Interfaces.InsertGuardView;
import view.Interfaces.MainView;
import view.Interfaces.RemoveGuardView;
import view.Interfaces.ShowPrisonersView;
import view.Interfaces.SupervisorFunctionsView;
import view.Interfaces.ViewGuardView;

public class SupervisorControllerImpl {

	private static SupervisorFunctionsView supervisorFunctionsView;

	private SupervisorControllerImpl(final SupervisorFunctionsView supervisorFunctionsView) {
		SupervisorControllerImpl.supervisorFunctionsView = supervisorFunctionsView;
		supervisorFunctionsView.addBackListener(new BackListener());
		supervisorFunctionsView.addShowPrisonersListener(new ShowPrisonersListener());
		supervisorFunctionsView.addInsertGuardListener(new InsertGuardListener());
		supervisorFunctionsView.addRemoveGuardListener(new RemoveGuardListener());
		supervisorFunctionsView.addviewGuardListener(new ViewGuardListener());
	}

	public static SupervisorControllerImpl createSupervisorControllerImpl(
			final SupervisorFunctionsView supervisorFunctionsView) {
		return new SupervisorControllerImpl(supervisorFunctionsView);
	}

	/**
	 * Listener to back previous view
	 */
	public static class BackListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			openDispose();
			MainControllerImpl.createMainControllerImpl(MainView.createMainView(supervisorFunctionsView.getRank()));
		}
	}

	/**
	 * Listener to InsertGuardView
	 */
	public static class InsertGuardListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			openDispose();
			InsertGuardControllerImpl.createInsertGuardControllerImpl(
					InsertGuardView.createInsertGuardView(supervisorFunctionsView.getRank()));
		}
	}

	/**
	 * Listener to RemoveGuardView
	 */
	public static class RemoveGuardListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			openDispose();
			RemoveGuardControllerImpl.createRemoveGuardControllerImpl(
					RemoveGuardView.createRemoveGuardView(supervisorFunctionsView.getRank()));
		}
	}

	/**
	 * Listener to ShowPrisonersView
	 */
	public static class ShowPrisonersListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			openDispose();
			ShowPrisonersControllerImpl.createShowPrisonersControllerImpl(
					ShowPrisonersView.createShowPrisonersView(supervisorFunctionsView.getRank()));
		}
	}

	/**
	 * Listener to ViewGuardView
	 */
	public static class ViewGuardListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			openDispose();
			ViewGuardControllerImpl.createViewGuardControllerImpl(
					ViewGuardView.createViewGuardView(supervisorFunctionsView.getRank()));
		}
	}

	private static void openDispose() {
		supervisorFunctionsView.dispose();
	}
}
