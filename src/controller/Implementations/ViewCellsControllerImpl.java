package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import controller.Interfaces.ViewCellsController;
import model.Implementations.CellImpl;
import utils.MyUtils;
import view.Interfaces.MoreFunctionsView;
import view.Interfaces.ViewCellsView;

public class ViewCellsControllerImpl implements ViewCellsController {

	private static ViewCellsView viewCellsView;
	private static final int WIDTH_FIRST_COLUMN = 20;
	private static final int WIDTH_SECOND_COLUMN = 200;
	private static final int CELL_POSITION = 0;
	private static final int DESCRIPTION_POSITION = 1;
	private static final int PRISONER_POSITION = 2;
	private static final int CAPACITY_POSITION = 3;

	private ViewCellsControllerImpl(final ViewCellsView viewCellsView) throws IOException {
		ViewCellsControllerImpl.viewCellsView = viewCellsView;
		viewCellsView.addBackListener(new BackListener());
		viewCellsView.createTableCells(getCellsTable());
	}

	public static ViewCellsControllerImpl createViewCellsControllerImpl(final ViewCellsView viewCellsView)
			throws IOException {
		return new ViewCellsControllerImpl(viewCellsView);
	}

	/**
	 * Listener to back previous view
	 */
	public static class BackListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent arg0) {
			viewCellsView.dispose();
			MoreFunctionsControllerImpl.createMoreFunctionsControllerImpl(
					MoreFunctionsView.createMoreFunctionsView(viewCellsView.getRank()));
		}
	}

	@Override
	public JTable getCellsTable() throws IOException {
		List<CellImpl> cellsList = new ArrayList<>();
		cellsList = InsertPrisonerControllerImpl.getCells();

		String[] cellsVector = { MyUtils.getCellId(), MyUtils.getDescription(), MyUtils.getActualPrisoners(),
				MyUtils.getMaxCellCapacity() };
		String[][] cellsMatrix = new String[cellsList.size()][cellsVector.length];
		for (int i = 0; i < cellsList.size(); i++) {
			cellsMatrix[i][CELL_POSITION] = String.valueOf(cellsList.get(i).getCellID());
			cellsMatrix[i][DESCRIPTION_POSITION] = cellsList.get(i).getPosition();
			cellsMatrix[i][PRISONER_POSITION] = String.valueOf(cellsList.get(i).getCurrentPrisoners());
			cellsMatrix[i][CAPACITY_POSITION] = String.valueOf(cellsList.get(i).getCapacity());
		}
		JTable cellsTable = new JTable(cellsMatrix, cellsVector);
		cellsTable.getColumnModel().getColumn(0).setPreferredWidth(WIDTH_FIRST_COLUMN);
		cellsTable.getColumnModel().getColumn(1).setPreferredWidth(WIDTH_SECOND_COLUMN);
		return cellsTable;
	}
}
