package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import controller.Interfaces.ViewPrisonerController;
import model.Interfaces.Prisoner;
import utils.MyUtils;
import view.Interfaces.MainView;
import view.Interfaces.ViewPrisonerView;

public class ViewPrisonerControllerImpl implements ViewPrisonerController {

	private static ViewPrisonerView viewPrisonerView;
	private static final String PRISONER_NOT_FOUND = "Prisoner not found";

	private ViewPrisonerControllerImpl(final ViewPrisonerView viewPrisonerView) {
		ViewPrisonerControllerImpl.viewPrisonerView = viewPrisonerView;
		viewPrisonerView.addViewListener(new ViewProfileListener());
		viewPrisonerView.addBackListener(new BackListener());
	}

	public static ViewPrisonerControllerImpl createViewPrisonerControllerImpl(final ViewPrisonerView viewPrisonerView) {
		return new ViewPrisonerControllerImpl(viewPrisonerView);
	}

	/**
	 * Listener to show prisoner
	 */
	public class ViewProfileListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent arg0) {
			showPrisoner();
		}
	}

	@Override
	public void showPrisoner() {
		boolean prisonerFound = false;
		if (String.valueOf(viewPrisonerView.getID()).isEmpty()) {
			viewPrisonerView.displayErrorMessage(MyUtils.getInvalidInput());
		}
		List<Prisoner> prisoners = new ArrayList<>();
		try {
			prisoners = MainControllerImpl.getPrisoners();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (final Prisoner prisoner : prisoners) {
			if (prisoner.getPrisonerID() == viewPrisonerView.getID()) {
				viewPrisonerView.setPrisonerProfile(prisoner.getName(), prisoner.getSurname(),
						prisoner.getBirthdate().toString(), prisoner.getStartPrisoning().toString(),
						prisoner.getEndPrisoning().toString());
				viewPrisonerView.setCrimesList(prisoner.getCrimes());
				prisonerFound = true;
			}
		}
		if (!prisonerFound) {
			viewPrisonerView.displayErrorMessage(PRISONER_NOT_FOUND);
		}
	}

	/**
	 * Listener to back previous view
	 */
	public static class BackListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			viewPrisonerView.dispose();
			MainControllerImpl.createMainControllerImpl(MainView.createMainView(viewPrisonerView.getRank()));
		}
	}
}
