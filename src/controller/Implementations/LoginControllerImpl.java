package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import model.Interfaces.Guard;
import utils.MyUtils;
import view.Interfaces.LoginView;
import view.Interfaces.MainView;

public class LoginControllerImpl {

	private final LoginView loginView;
	private static final String WELCOME = "Welcome user ";
	private static final String INSERT_ERROR = "Insert username and password";
	private static final String LOGIN_ERROR = "Wrong username or password";
	private static final String FIRST_PROFILE = "\n ID: 3 - Password: qwerty";
	private static final String SECOND_PROFILE = "\n ID: 2 - Password: asdasd";
	private static final String ACCESS_PROFILE = "Access with profile: ";

	private LoginControllerImpl(final LoginView loginView) {
		this.loginView = loginView;
		loginView.addLoginListener(new LoginListener());
		loginView.displayErrorMessage(ACCESS_PROFILE + FIRST_PROFILE + SECOND_PROFILE);
	}

	public static LoginControllerImpl createLoginControllerImpl(final LoginView loginView) {
		return new LoginControllerImpl(loginView);
	}

	/**
	 * Listener to login
	 */
	public class LoginListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			boolean isGuardInside = false;
			List<Guard> guards = new ArrayList<>();
			try {
				guards = getGuards();
			} catch (final ClassNotFoundException | IOException exception) {
				exception.printStackTrace();
			}
			checkLogin(isGuardInside, guards);
		}

		private void checkLogin(boolean isGuardInside, final List<Guard> guards) {
			if (loginView.getUsername().isEmpty() || loginView.getPassword().isEmpty()) {
				loginView.displayErrorMessage(INSERT_ERROR);
			} else {
				for (final Guard guard : guards) {
					if (loginView.getUsername().equals(String.valueOf(guard.getUsername()))
							&& loginView.getPassword().equals(guard.getPassword())) {
						isGuardInside = true;
						loginView.displayErrorMessage(WELCOME + loginView.getUsername());
						loginView.dispose();
						MainControllerImpl.createMainControllerImpl(MainView.createMainView(guard.getRank()));
					}
				}
				if (isGuardInside == false) {
					loginView.displayErrorMessage(LOGIN_ERROR);
					loginView.setPassword();
					loginView.setUsername();
				}
				isGuardInside = false;
			}
		}
	}

	/**
	 * @return Guards' list
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static List<Guard> getGuards() throws IOException, ClassNotFoundException {
		File guardsFile = new File(MyUtils.getGuardsFile());
		if (guardsFile.length() != 0) {
			FileInputStream fileInputStream = new FileInputStream(guardsFile);
			ObjectInputStream objectOutputStream = new ObjectInputStream(fileInputStream);
			List<Guard> guards = new ArrayList<>();
			try {
				while (true) {
					Guard guard = (Guard) objectOutputStream.readObject();
					guards.add(guard);
				}
			} catch (final EOFException endOfFileException) {
			}
			fileInputStream.close();
			objectOutputStream.close();
			return guards;
		}
		return new ArrayList<Guard>();
	}
}
