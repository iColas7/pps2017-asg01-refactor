package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import model.Interfaces.Guard;
import utils.MyUtils;
import view.Interfaces.SupervisorFunctionsView;
import view.Interfaces.ViewGuardView;

public class ViewGuardControllerImpl {

	private static ViewGuardView viewGuardView;

	private ViewGuardControllerImpl(final ViewGuardView viewGuardView) {
		ViewGuardControllerImpl.viewGuardView = viewGuardView;
		viewGuardView.addBackListener(new BackListener());
		viewGuardView.addViewListener(new ViewGuardListener());
	}

	public static ViewGuardControllerImpl createViewGuardControllerImpl(final ViewGuardView viewGuardView) {
		return new ViewGuardControllerImpl(viewGuardView);
	}

	/**
	 * Listener to select guard
	 */
	public static class ViewGuardListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			List<Guard> guardsList = new ArrayList<>();
			boolean guardFound = false;
			try {
				guardsList = LoginControllerImpl.getGuards();
			} catch (ClassNotFoundException | IOException e1) {
				e1.printStackTrace();
			}
			viewGuards(guardsList, guardFound);
		}
	}

	/**
	 * @param guardsList
	 *            Guards' list
	 * @param guardFound
	 *            true if guard founded, false otherwise
	 */
	public static void viewGuards(final List<Guard> guardsList, boolean guardFound) {
		for (final Guard guard : guardsList) {
			if (guard.getID() == viewGuardView.getID()) {
				viewGuardView.setName(guard.getName());
				viewGuardView.setSurname(guard.getSurname());
				viewGuardView.setBirth(guard.getBirthdate().toString());
				viewGuardView.setRank(String.valueOf(guard.getRank()));
				viewGuardView.setTelephone(guard.getTelephoneNumber());
				guardFound = true;
			}
		}
		if (!guardFound)
			viewGuardView.displayErrorMessage(MyUtils.getGuardNotFound());
	}

	/**
	 * Listener to back previous view
	 */
	public static class BackListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			viewGuardView.dispose();
			SupervisorControllerImpl.createSupervisorControllerImpl(
					SupervisorFunctionsView.createSupervisorFunctionsView(viewGuardView.getRank()));
		}
	}
}
