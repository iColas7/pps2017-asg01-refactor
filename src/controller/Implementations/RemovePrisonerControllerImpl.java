package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import controller.Interfaces.RemovePrisonerController;
import model.Implementations.CellImpl;
import model.Interfaces.Prisoner;
import utils.MyUtils;
import view.Interfaces.MainView;
import view.Interfaces.RemovePrisonerView;

public class RemovePrisonerControllerImpl implements RemovePrisonerController {

	private static RemovePrisonerView removePrisonerView;
	private static final String REMOVED_PRISONER = "Removed prisoner";
	private static final String PRISONER_NOT_FOUND = "Prisoner not found";

	private RemovePrisonerControllerImpl(final RemovePrisonerView removePrisonerView) {
		RemovePrisonerControllerImpl.removePrisonerView = removePrisonerView;
		removePrisonerView.addRemoveListener(new RemoveListener());
		removePrisonerView.addBackListener(new BackListener());
	}

	public static RemovePrisonerControllerImpl createRemovePrisonerControllerImpl(
			final RemovePrisonerView removePrisonerView) {
		return new RemovePrisonerControllerImpl(removePrisonerView);
	}

	/**
	 * Listener to remove prisoner
	 */
	public class RemoveListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {

			try {
				removePrisoner();
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}

		}
	}

	@Override
	public void removePrisoner() throws IOException, ClassNotFoundException {

		boolean prisonerFound = false;
		List<Prisoner> currentPrisoners = new ArrayList<>();
		currentPrisoners = MainControllerImpl.getCurrentPrisoners();

		for (final Prisoner prisoner : currentPrisoners) {

			if (prisoner.getPrisonerID() == removePrisonerView.getID()) {
				currentPrisoners.remove(prisoner);
				removePrisonerView.displayErrorMessage(REMOVED_PRISONER);
				List<CellImpl> cellsList = InsertPrisonerControllerImpl.getCells();

				for (final CellImpl cell : cellsList) {

					if (prisoner.getCellID() == cell.getCellID()) {
						cell.setCurrentPrisoners(cell.getCurrentPrisoners() - 1);
					}
				}

				InsertPrisonerControllerImpl.setCells(cellsList);
				prisonerFound = true;
				break;
			}
		}

		File currentPrisonersFile = new File(MyUtils.getActualPrisonersFile());
		FileOutputStream fileOutputStream = new FileOutputStream(currentPrisonersFile);
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
		objectOutputStream.flush();

		for (final Prisoner prisoner : currentPrisoners) {
			objectOutputStream.writeObject(prisoner);
		}

		objectOutputStream.close();
		fileOutputStream.close();
		if (!prisonerFound) {
			removePrisonerView.displayErrorMessage(PRISONER_NOT_FOUND);
		}
	}

	/**
	 * Listener to back previous view
	 */
	public class BackListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			removePrisonerView.dispose();
			MainControllerImpl.createMainControllerImpl(MainView.createMainView(removePrisonerView.getRank()));
		}
	}
}