package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;
import javax.swing.JTable;
import controller.Interfaces.BalanceController;
import model.Implementations.MovementImpl;
import utils.MyUtils;
import view.Interfaces.BalanceView;
import view.Interfaces.MoreFunctionsView;

public class BalanceControllerImpl implements BalanceController {

	private static final int SIGN = 0;
	private static final int AMOUNT = 1;
	private static final int DESCRIPTION = 2;
	private static final int DATE = 3;
	private static final char PLUS = '+';
	private static final char MINUS = '-';
	private static BalanceView balanceView;

	private BalanceControllerImpl(final BalanceView balanceView) throws IOException {
		BalanceControllerImpl.balanceView = balanceView;
		balanceView.addBackListener(new BackListener());
		showBalance();
	}

	public static BalanceControllerImpl createBalanceControllerImpl(final BalanceView balanceView) throws IOException {
		return new BalanceControllerImpl(balanceView);
	}

	/**
	 * Method to return back in view
	 */
	public class BackListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			balanceView.dispose();
			MoreFunctionsControllerImpl.createMoreFunctionsControllerImpl(
					MoreFunctionsView.createMoreFunctionsView(balanceView.getRank()));
		}

	}

	@Override
	public void showBalance() throws IOException {
		List<MovementImpl> movementList = AddMovementControllerImpl.InsertListener.getMovements();
		calculateBalance(movementList);
		String[] elements = { MyUtils.getElementSign(), MyUtils.getElementAmount(), MyUtils.getElementDescription(),
				MyUtils.getElementDate() };
		String[][] movementMatrix = calculateBalanceMatrix(movementList, elements);
		JTable balanceTable = new JTable(movementMatrix, elements);
		balanceView.createBalanceTable(balanceTable);
	}

	private void calculateBalance(final List<?> movementList) {
		int balance = 0;
		for (final Object movement : movementList) {
			switch (((MovementImpl) movement).getPlusOrMinusMovement()) {
			case MINUS:
				balance -= ((MovementImpl) movement).getAmount();
				break;
			case PLUS:
				balance += ((MovementImpl) movement).getAmount();
				break;
			}
			balanceView.setBalanceLabel(String.valueOf(balance));
		}
	}

	private String[][] calculateBalanceMatrix(final List<?> movementList, final String[] elements) {
		String[][] movementMatrix = new String[movementList.size()][elements.length];
		for (int i = 0; i < movementList.size(); i++) {
			movementMatrix[i][SIGN] = String.valueOf(((MovementImpl) movementList.get(i)).getPlusOrMinusMovement());
			movementMatrix[i][AMOUNT] = String.valueOf(((MovementImpl) movementList.get(i)).getAmount());
			movementMatrix[i][DESCRIPTION] = ((MovementImpl) movementList.get(i)).getMovementDescription();
			movementMatrix[i][DATE] = ((MovementImpl) movementList.get(i)).getMovementDate().toString();
		}
		return movementMatrix;
	}
}
