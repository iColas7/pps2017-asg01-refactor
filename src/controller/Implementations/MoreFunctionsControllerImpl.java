package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JFrame;

import controller.Interfaces.MoreFunctionsController;
import model.Interfaces.Prisoner;
import utils.MyUtils;
import view.Components.PrisonersForYearChart;
import view.Components.CrimesPrisonersChart;
import view.Interfaces.AddMovementView;
import view.Interfaces.AddPolicemanView;
import view.Interfaces.AddVisitorsView;
import view.Interfaces.BalanceView;
import view.Interfaces.MainView;
import view.Interfaces.MoreFunctionsView;
import view.Interfaces.ViewCellsView;
import view.Interfaces.ViewPolicemanView;
import view.Interfaces.ViewVisitorsView;

public class MoreFunctionsControllerImpl implements MoreFunctionsController {

	private MoreFunctionsView moreFunctionsView;
	private final static int OPENING_PRISON = 2017;

	private MoreFunctionsControllerImpl(final MoreFunctionsView moreFunctionsView) {
		this.moreFunctionsView = moreFunctionsView;
		this.moreFunctionsView.addBackListener(new BackListener());
		this.moreFunctionsView.addAddMovementListener(new AddMovementListener());
		this.moreFunctionsView.addBalanceListener(new BalanceListener());
		this.moreFunctionsView.addChartPrisonersForYear(new OpenChartPrisonersForYear());
		this.moreFunctionsView.addChartCrimesPercent(new OpenChartPercentCrimes());
		this.moreFunctionsView.addAddVisitorsListener(new AddVisitorsListener());
		this.moreFunctionsView.addViewVisitorsListener(new ViewVisitorsListener());
		this.moreFunctionsView.addAddPolicemanListener(new AddPolicemanListener());
		this.moreFunctionsView.addViewPolicemanListener(new ViewPolicemanListener());
		this.moreFunctionsView.addViewCellsListener(new ViewCellsListener());
	}

	public static MoreFunctionsControllerImpl createMoreFunctionsControllerImpl(
			final MoreFunctionsView moreFunctionsView) {
		return new MoreFunctionsControllerImpl(moreFunctionsView);
	}

	/**
	 * Listener to back previous page
	 */
	public class BackListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent arg0) {
			callDisposeView();
			MainControllerImpl.createMainControllerImpl(MainView.createMainView(moreFunctionsView.getRank()));
		}
	}

	/**
	 * Listener to open AddMovementView
	 */
	public class AddMovementListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent arg0) {
			callDisposeView();
			AddMovementControllerImpl.createAddMovementControllerImpl(
					AddMovementView.createAddMovementView(moreFunctionsView.getRank()));
		}
	}

	/**
	 * Listener to open BalanceView
	 */
	public class BalanceListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			callDisposeView();
			try {
				BalanceControllerImpl
						.createBalanceControllerImpl(BalanceView.createBalanceView(moreFunctionsView.getRank()));
			} catch (final IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	@Override
	public void createChartPrisonersForYear() {
		Map<Integer, Integer> prisonersMap = new TreeMap<>();
		List<Prisoner> prisonersList = new ArrayList<>();
		try {
			prisonersList = MainControllerImpl.getPrisoners();
		} catch (ClassNotFoundException | IOException e1) {
			e1.printStackTrace();
		}
		int maxYearPrisoning = getMaxYearEndPrisoning(prisonersList);
		for (int i = OPENING_PRISON; i <= maxYearPrisoning; i++) {
			int numberOfPrisonerForYear = 0;
			for (final Prisoner prisoner : prisonersList) {
				Calendar startPrisoning = Calendar.getInstance();
				Calendar endPrisoning = Calendar.getInstance();
				startPrisoning.setTime(prisoner.getStartPrisoning());
				endPrisoning.setTime(prisoner.getEndPrisoning());
				if (startPrisoning.get(Calendar.YEAR) <= i && endPrisoning.get(Calendar.YEAR) >= i) {
					numberOfPrisonerForYear++;
				}
			}
			prisonersMap.put(i, numberOfPrisonerForYear);
		}
		PrisonersForYearChart chart = new PrisonersForYearChart(prisonersMap, MyUtils.getPrisonersForYear(),
				MyUtils.getPrisonersForYear());
		chart.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	@Override
	public int getMaxYearEndPrisoning(final List<Prisoner> prisonersList) {
		int maxYearPrisoning = 0;
		for (final Prisoner prisoner : prisonersList) {
			Calendar endPrisoning = Calendar.getInstance();
			endPrisoning.setTime(prisoner.getEndPrisoning());
			if (endPrisoning.get(Calendar.YEAR) > maxYearPrisoning) {
				maxYearPrisoning = endPrisoning.get(Calendar.YEAR);
			}
		}
		return maxYearPrisoning;
	}

	/**
	 * Listener to open first chart
	 */
	public class OpenChartPrisonersForYear implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			createChartPrisonersForYear();
		}
	}

	@Override
	public void createChartCrimesPercent() {
		String[] crimes = { MyUtils.getCrimesAgainstAnimals(), MyUtils.getCrimesAssociatives(),
				MyUtils.getBlasphemyAndSacrilege(), MyUtils.getFinancialAndEconomicsCrimes(),
				MyUtils.getFalseTestimony(), MyUtils.getMilitaryCrimes(), MyUtils.getHeritageCrimes(),
				MyUtils.getPersonCrimes(), MyUtils.getSystemCrimes(), MyUtils.getTaxCrimes(), MyUtils.getDrugCrimes(),
				MyUtils.getScamCases() };
		ArrayList<String> crimesList = new ArrayList<>(Arrays.asList(crimes));
		List<Prisoner> prisonersList = new ArrayList<>();
		try {
			prisonersList = MainControllerImpl.getCurrentPrisoners();
		} catch (ClassNotFoundException | IOException e1) {
			e1.printStackTrace();
		}
		Map<String, Integer> crimesMap = new HashMap<>();
		for (final String crime : crimesList) {
			crimesMap.put(crime, 0);
		}
		for (final Prisoner prisoner : prisonersList) {
			for (final String prisonerCrime : prisoner.getCrimes()) {
				if (crimesList.contains(prisonerCrime)) {
					crimesMap.put(prisonerCrime, crimesMap.get(prisonerCrime) + 1);
				}
			}
		}
		CrimesPrisonersChart chartPercentCrimes = new CrimesPrisonersChart(MyUtils.getPercentCrimes(), crimesMap);
		chartPercentCrimes.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	/**
	 * Listener to open second chart
	 */
	public class OpenChartPercentCrimes implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			createChartCrimesPercent();
		}
	}

	/**
	 * Listener to open AddVisitorsView
	 */
	public class AddVisitorsListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			callDisposeView();
			AddVisitorsControllerImpl.createAddVisitorsControllerImpl(
					AddVisitorsView.createAddVisitorsView(moreFunctionsView.getRank()));
		}
	}

	/**
	 * Listener to open VisitorsViews
	 */
	public class ViewVisitorsListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			callDisposeView();
			ViewVisitorsControllerImpl.createViewVisitorsControllerImpl(
					ViewVisitorsView.createViewVisitorsView(moreFunctionsView.getRank()));
		}
	}
	
	/**
	 * Listener to open AddVisitorsView
	 */
	public class AddPolicemanListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			callDisposeView();
			AddPolicemenControllerImpl.createAddPolicemanControllerImpl(
					AddPolicemanView.createAddPolicemanView(moreFunctionsView.getRank()));
		}
	}

	/**
	 * Listener to open VisitorsViews
	 */
	public class ViewPolicemanListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			callDisposeView();
			ViewPolicemenControllerImpl.createViewPolicemanControllerImpl(
					ViewPolicemanView.createViewPolicemanView(moreFunctionsView.getRank()));
		}
	}

	/**
	 * Listener to open CellsViews
	 */
	public class ViewCellsListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			callDisposeView();
			try {
				ViewCellsControllerImpl
						.createViewCellsControllerImpl(ViewCellsView.createViewCellsView(moreFunctionsView.getRank()));
			} catch (final IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	private void callDisposeView() {
		moreFunctionsView.dispose();
	}
}
