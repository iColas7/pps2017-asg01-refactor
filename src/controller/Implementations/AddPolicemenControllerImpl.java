package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

import model.Interfaces.Policeman;
import model.Interfaces.Prisoner;
import utils.MyUtils;
import view.Interfaces.AddPolicemanView;
import view.Interfaces.MoreFunctionsView;

public class AddPolicemenControllerImpl {

	private static AddPolicemanView policemanView;
	private static final int MIN_LENGTH_NAME_AND_SURNAME = 2;

	private AddPolicemenControllerImpl(final AddPolicemanView view) {
		AddPolicemenControllerImpl.policemanView = view;
		policemanView.addBackListener(new BackListener());
		policemanView.addInsertPolicemanListener(new InsertListener());
	}

	public static AddPolicemenControllerImpl createAddPolicemanControllerImpl(final AddPolicemanView view) {
		return new AddPolicemenControllerImpl(view);
	}

	/**
	 * Listener back to previous page
	 */
	public class BackListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			policemanView.dispose();
			MoreFunctionsControllerImpl.createMoreFunctionsControllerImpl(
					MoreFunctionsView.createMoreFunctionsView(policemanView.getRank()));
		}
	}

	/**
	 * Listener to manage insert policeman
	 */
	public static class InsertListener implements ActionListener {

		@Override
		public void actionPerformed(final ActionEvent arg0) {

			List<Policeman> policemanList = new ArrayList<>();
			try {
				policemanList = getPoliceman();
			} catch (final ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}

			Policeman policeman = policemanView.getPoliceman();

			try {
				if (policeman.getName().length() < MIN_LENGTH_NAME_AND_SURNAME
						|| policeman.getSurname().length() < MIN_LENGTH_NAME_AND_SURNAME || !checkPrisonerID(policeman))
					policemanView.displayErrorMessage(MyUtils.getCorrectDate());
				else {
					policemanList.add(policeman);
					policemanView.displayErrorMessage("Policeman inserted");
				}
			} catch (final ClassNotFoundException classNotFoundException) {
				classNotFoundException.printStackTrace();
			} catch (final IOException ioException) {
				ioException.printStackTrace();
			}
			try {
				setPoliceman(policemanList);
			} catch (final IOException e) {
				e.printStackTrace();
			}

		}

		/**
		 * @return Policemen' list
		 * @throws IOException
		 * @throws ClassNotFoundException
		 */
		public static List<Policeman> getPoliceman() throws IOException, ClassNotFoundException {
			File policemanFile = new File(MyUtils.getAllPolicemenFile());
			if (policemanFile.length() != 0) {
				FileInputStream fileInputStream = new FileInputStream(policemanFile);
				ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
				List<Policeman> policemanList = new ArrayList<>();
				try {
					while (true) {
						Policeman policeman = (Policeman) objectInputStream.readObject();
						policemanList.add(policeman);
					}
				} catch (final EOFException eofe) {
				}
				fileInputStream.close();
				objectInputStream.close();
				return policemanList;
			}
			return new ArrayList<Policeman>();
		}
	}

	/**
	 * Check prisoner's ID
	 * 
	 * @param policeman
	 *            Policeman
	 * @return true If ID is correct
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public static boolean checkPrisonerID(final Policeman policeman) throws ClassNotFoundException, IOException {
		List<Prisoner> prisonersList = MainControllerImpl.getCurrentPrisoners();
		boolean foundPrisoner = false;
		for (final Prisoner prisoner : prisonersList) {
			if (prisoner.getPrisonerID() == policeman.getPrisonerID()) {
				foundPrisoner = true;
				return foundPrisoner;
			} else
				continue;
		}
		return false;
	}

	/**
	 * Set updated policemen' list
	 * 
	 * @param policemen
	 *            Policemen' list
	 * @throws IOException
	 */
	public static void setPoliceman(final List<Policeman> policemen) throws IOException {
		File policemanFile = new File(MyUtils.getAllPolicemenFile());
		FileOutputStream fileOutputStream = new FileOutputStream(policemanFile);
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

		for (final Policeman policeman : policemen) {
			try {
				objectOutputStream.writeObject(policeman);
			} catch (final IOException ioException) {
				ioException.printStackTrace();
			}
		}

		objectOutputStream.close();
		fileOutputStream.close();
	}
}
