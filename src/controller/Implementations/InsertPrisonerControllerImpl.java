package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import controller.Interfaces.InsertPrisonerController;
import model.Implementations.CellImpl;
import model.Implementations.PrisonerImplBuilder;
import model.Interfaces.Prisoner;
import utils.MyUtils;
import view.Interfaces.InsertPrisonerView;
import view.Interfaces.MainView;

public class InsertPrisonerControllerImpl implements InsertPrisonerController {

	private static InsertPrisonerView insertPrisonerView;
	private static final int MAX_CELL = 49;

	private InsertPrisonerControllerImpl(final InsertPrisonerView insertPrisonerView) {
		InsertPrisonerControllerImpl.insertPrisonerView = insertPrisonerView;
		insertPrisonerView.addInsertPrisonerListener(new InsertPrisonerListener());
		insertPrisonerView.addBackListener(new BackListener());
		insertPrisonerView.addAddCrimeListener(new AddCrimeListener());
	}

	public static InsertPrisonerControllerImpl createInsertPrisonerControllerImpl(
			final InsertPrisonerView insertPrisonerView) {
		return new InsertPrisonerControllerImpl(insertPrisonerView);
	}

	@Override
	public void insertPrisoner() throws IOException {
		boolean prisonerInsertedError = true;
		List<Prisoner> prisonersList = new ArrayList<>();
		List<Prisoner> currentPrisoners = new ArrayList<>();

		try {
			prisonersList = MainControllerImpl.getPrisoners();
			currentPrisoners = MainControllerImpl.getCurrentPrisoners();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Prisoner prisoner = new PrisonerImplBuilder().setName(insertPrisonerView.getPrisonerName())
				.setSurname(insertPrisonerView.getPrisonerSurname())
				.setBirthdate(insertPrisonerView.getPrisonerBirthday())
				.setPrisonerID(insertPrisonerView.getPrisonerID())
				.setStartPrisoning(insertPrisonerView.getStartPrisoning())
				.setEndPrisoning(insertPrisonerView.getEndPrisoning())
				.setListOfCrimes(insertPrisonerView.getCrimesList()).setCellID(insertPrisonerView.getCellID())
				.createPrisonerImpl();

		checkPrisoner(prisoner, prisonersList, prisonerInsertedError, currentPrisoners);

	}

	private void checkPrisoner(final Prisoner prisoner, final List<Prisoner> prisonersList,
			boolean prisonerInsertedError, final List<Prisoner> currentPrisoners) throws IOException {
		if (isSomethingEmpty(prisoner)) {
			insertPrisonerView.displayErrorMessage(MyUtils.getCompleteFields());
		} else {

			for (final Prisoner actualPrisoner : prisonersList) {
				if ((actualPrisoner).getPrisonerID() == prisoner.getPrisonerID()) {
					insertPrisonerView.displayErrorMessage(MyUtils.getIdAlreadyUsed());
					prisonerInsertedError = false;
				}
			}

			Calendar today = Calendar.getInstance();
			today.set(Calendar.HOUR_OF_DAY, 0);

			if (prisonerInsertedError == true) {
				if (prisoner.getStartPrisoning().after(prisoner.getEndPrisoning())
						|| prisoner.getStartPrisoning().before(today.getTime())
						|| prisoner.getBirthdate().after(today.getTime())) {
					insertPrisonerView.displayErrorMessage(MyUtils.getCorrectDate());
				} else {
					prisonersList.add(prisoner);
					currentPrisoners.add(prisoner);
					List<CellImpl> cellsList = getCells();

					for (final CellImpl cell : cellsList) {
						if (prisoner.getCellID() == cell.getCellID() || prisoner.getCellID() < 0
								|| prisoner.getCellID() > MAX_CELL) {
							if (cell.getCapacity() == cell.getCurrentPrisoners()) {
								insertPrisonerView.displayErrorMessage(MyUtils.getAnotherCell());
								return;
							}
							cell.setCurrentPrisoners(cell.getCurrentPrisoners() + 1);
						}
					}
					setCells(cellsList);
					setPrisoners(prisonersList, currentPrisoners);
					insertPrisonerView.displayErrorMessage(MyUtils.getPrisonerInserted());
					insertPrisonerView.setCrimesList(new ArrayList<String>());
				}
			}
		}
	}

	/**
	 * Listener to insert prisoners
	 */
	public class InsertPrisonerListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			try {
				insertPrisoner();
			} catch (final IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Listener to back previuos page
	 */
	public class BackListener implements ActionListener {

		@Override
		public void actionPerformed(final ActionEvent arg0) {
			insertPrisonerView.dispose();
			MainControllerImpl.createMainControllerImpl(MainView.createMainView(insertPrisonerView.getRank()));
		}

	}

	/**
	 * Listener to add crime at crimes' list of prisoner
	 */
	public class AddCrimeListener implements ActionListener {

		@Override
		public void actionPerformed(final ActionEvent arg0) {
			List<String> crimesList = insertPrisonerView.getCrimesList();
			if (crimesList.contains(insertPrisonerView.getCrimeSelected())) {
				insertPrisonerView.displayErrorMessage(MyUtils.getCrimeAlreadyPresent());
			} else {
				crimesList.add(insertPrisonerView.getCrimeSelected());
				insertPrisonerView.setCrimesList(crimesList);
			}
		}
	}

	@Override
	public boolean isSomethingEmpty(final Prisoner prisoner) {
		if (prisoner.getName().isEmpty() || prisoner.getSurname().isEmpty() || prisoner.getCrimes().size() == 1) {
			return true;
		}
		return false;
	}

	/**
	 * @return Cells' list
	 * @throws IOException
	 */
	public static List<CellImpl> getCells() throws IOException {
		File cellsFile = new File(MyUtils.getCellsFile());
		List<CellImpl> cellsList = new ArrayList<>();
		FileInputStream fileInputStream = new FileInputStream(cellsFile);
		ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

		try {
			while (true) {
				CellImpl actualCell = (CellImpl) objectInputStream.readObject();
				cellsList.add(actualCell);
			}

		} catch (EOFException endOfFileException) {
		} catch (ClassNotFoundException classNotFoundException) {
			classNotFoundException.printStackTrace();
		} catch (IOException ioException) {
			ioException.printStackTrace();
		}

		objectInputStream.close();
		return cellsList;
	}

	/**
	 * Set update cells' list
	 * 
	 * @param cellsList
	 *            Cells' list
	 * @throws IOException
	 */
	public static void setCells(final List<CellImpl> cellsList) throws IOException {
		File cellsFile = new File(MyUtils.getCellsFile());
		FileOutputStream fileOutputStream = new FileOutputStream(cellsFile);
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
		objectOutputStream.flush();
		fileOutputStream.flush();
		for (final CellImpl cell : cellsList) {
			objectOutputStream.writeObject(cell);
		}
		objectOutputStream.close();
	}

	/**
	 * Set prisoners' list
	 * 
	 * @param prisoners
	 *            All prisoners
	 * @param currentPrisoners
	 *            Actual prisoners
	 * @throws IOException
	 */
	public static void setPrisoners(final List<Prisoner> prisoners, final List<Prisoner> currentPrisoners)
			throws IOException {

		File prisonersFile = new File(MyUtils.getPrisonersFile());
		File actualPrisonersFile = new File(MyUtils.getActualPrisonersFile());
		FileOutputStream fileOutputStreamPrisoners = new FileOutputStream(prisonersFile);
		FileOutputStream fileOutputStreamActualPrisoners = new FileOutputStream(actualPrisonersFile);
		ObjectOutputStream objectOutputStreamPrisoners = new ObjectOutputStream(fileOutputStreamPrisoners);
		ObjectOutputStream objectOutputStreamActualPrisoners = new ObjectOutputStream(fileOutputStreamActualPrisoners);

		for (final Prisoner singlePrisoner : prisoners) {
			try {
				objectOutputStreamPrisoners.writeObject(singlePrisoner);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		for (final Prisoner actualPrisoner : currentPrisoners) {
			try {
				objectOutputStreamActualPrisoners.writeObject(actualPrisoner);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		objectOutputStreamPrisoners.close();
		objectOutputStreamActualPrisoners.close();
		fileOutputStreamPrisoners.close();
		fileOutputStreamActualPrisoners.close();
	}
}
