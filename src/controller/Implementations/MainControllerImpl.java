package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import model.Interfaces.Prisoner;
import utils.MyUtils;
import view.Interfaces.InsertPrisonerView;
import view.Interfaces.LoginView;
import view.Interfaces.MainView;
import view.Interfaces.MoreFunctionsView;
import view.Interfaces.RemovePrisonerView;
import view.Interfaces.SupervisorFunctionsView;
import view.Interfaces.ViewPrisonerView;

public class MainControllerImpl {

	private final MainView mainView;

	private MainControllerImpl(final MainView mainView) {
		this.mainView = mainView;
		this.mainView.addLogoutListener(new LogoutListener());
		this.mainView.addInsertPrisonerListener(new InsertPrisonerListener());
		this.mainView.addRemovePrisonerListener(new RemovePrisonerListener());
		this.mainView.addViewPrisonerListener(new ViewPrisonerListener());
		this.mainView.addMoreFunctionsListener(new MoreFunctionsListener());
		this.mainView.addSupervisorListener(new SupervisorListener());
	}

	public static MainControllerImpl createMainControllerImpl(final MainView mainView) {
		return new MainControllerImpl(mainView);
	}

	/**
	 * Listener to back previous page
	 */
	public class LogoutListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent arg0) {
			callDispose();
			LoginControllerImpl.createLoginControllerImpl(LoginView.createLoginView());
		}
	}

	/**
	 * Listener to go InsertPrisonerViews
	 */
	public class InsertPrisonerListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent arg0) {
			callDispose();
			InsertPrisonerControllerImpl.createInsertPrisonerControllerImpl(
					InsertPrisonerView.createInsertPrisonerView(mainView.getRank()));
		}
	}

	/**
	 * Listener to go RemovePrisonerView
	 */
	public class RemovePrisonerListener implements ActionListener {

		@Override
		public void actionPerformed(final ActionEvent arg0) {
			callDispose();
			RemovePrisonerControllerImpl.createRemovePrisonerControllerImpl(
					RemovePrisonerView.createRemovePrisonerView(mainView.getRank()));
		}
	}

	/**
	 * Listener to go ViewPrisonerView
	 */
	public class ViewPrisonerListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent arg0) {
			callDispose();
			ViewPrisonerControllerImpl
					.createViewPrisonerControllerImpl(ViewPrisonerView.createViewPrisonerView(mainView.getRank()));
		}
	}

	/**
	 * 
	 * @return Prisoners' list
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static List<Prisoner> getPrisoners() throws IOException, ClassNotFoundException {
		File prisonersFile = new File(MyUtils.getPrisonersFile());
		List<Prisoner> prisoners = new ArrayList<>();
		if (prisonersFile.length() != 0) {
			FileInputStream fileInputStream = new FileInputStream(prisonersFile);
			ObjectInputStream objectOutputStream = new ObjectInputStream(fileInputStream);
			try {
				while (true) {
					Prisoner prisoner = (Prisoner) objectOutputStream.readObject();
					prisoners.add(prisoner);
				}
			} catch (EOFException endOfFileException) {
			}
			fileInputStream.close();
			objectOutputStream.close();
		}
		return prisoners;
	}

	/**
	 * @return Actual prisoners' list
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static List<Prisoner> getCurrentPrisoners() throws IOException, ClassNotFoundException {
		File actualPrisonersFile = new File(MyUtils.getActualPrisonersFile());
		List<Prisoner> prisoners = new ArrayList<>();
		if (actualPrisonersFile.length() != 0) {
			FileInputStream fileInputStream = new FileInputStream(actualPrisonersFile);
			ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
			try {
				while (true) {
					Prisoner prisoner = (Prisoner) objectInputStream.readObject();
					prisoners.add(prisoner);
				}
			} catch (EOFException endOfFileException) {
			}
			fileInputStream.close();
			objectInputStream.close();
		}
		return prisoners;
	}

	/**
	 * Listener go to MoreFunctionView
	 */
	public class MoreFunctionsListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent arg0) {
			callDispose();
			MoreFunctionsControllerImpl
					.createMoreFunctionsControllerImpl(MoreFunctionsView.createMoreFunctionsView(mainView.getRank()));
		}
	}

	/**
	 * Listener go to SupervisorFunctionView
	 */
	public class SupervisorListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent arg0) {
			callDispose();
			SupervisorControllerImpl.createSupervisorControllerImpl(
					SupervisorFunctionsView.createSupervisorFunctionsView(mainView.getRank()));
		}
	}

	private void callDispose() {
		mainView.dispose();
	}
}
