package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import controller.Interfaces.RemoveGuardController;
import model.Interfaces.Guard;
import utils.MyUtils;
import view.Interfaces.RemoveGuardView;
import view.Interfaces.SupervisorFunctionsView;

public class RemoveGuardControllerImpl implements RemoveGuardController {

	private static RemoveGuardView removeGuardView;
	private static final String REMOVED_GUARD = "Removed guard";

	private RemoveGuardControllerImpl(final RemoveGuardView removeGuardView) {
		RemoveGuardControllerImpl.removeGuardView = removeGuardView;
		removeGuardView.addBackListener(new BackListener());
		removeGuardView.addRemoveGuardListener(new RemoveGuardListener());
	}

	public static RemoveGuardControllerImpl createRemoveGuardControllerImpl(final RemoveGuardView removeGuardView) {
		return new RemoveGuardControllerImpl(removeGuardView);
	}

	/**
	 * Listener to remove guard
	 */
	public class RemoveGuardListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			try {
				removeGuard();
			} catch (final IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	@Override
	public void removeGuard() throws IOException {
		boolean guardFound = false;
		List<Guard> guards = new ArrayList<>();
		try {
			guards = LoginControllerImpl.getGuards();
		} catch (final ClassNotFoundException | IOException e1) {
			e1.printStackTrace();
		}

		for (final Guard guard : guards) {
			if (guard.getID() == removeGuardView.getID()) {
				guards.remove(guard);
				removeGuardView.displayErrorMessage(REMOVED_GUARD);
				guardFound = true;
				break;
			}
		}

		File updatedGuardsFile = new File(MyUtils.getGuardsFile());
		FileOutputStream fileOutputStream = new FileOutputStream(updatedGuardsFile);
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
		objectOutputStream.flush();

		for (final Guard guard : guards) {
			objectOutputStream.writeObject(guard);
		}

		objectOutputStream.close();
		fileOutputStream.close();
		if (!guardFound) {
			removeGuardView.displayErrorMessage(MyUtils.getGuardNotFound());
		}
	}

	/**
	 * Listener to back previous view
	 */
	public static class BackListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent arg0) {
			removeGuardView.dispose();
			SupervisorControllerImpl.createSupervisorControllerImpl(
					SupervisorFunctionsView.createSupervisorFunctionsView(removeGuardView.getRank()));
		}

	}
}
