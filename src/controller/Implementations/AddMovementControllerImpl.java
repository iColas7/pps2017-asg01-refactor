package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import model.Implementations.MovementImpl;
import utils.MyUtils;
import view.Interfaces.AddMovementView;
import view.Interfaces.MoreFunctionsView;

public class AddMovementControllerImpl {

	private static AddMovementView addMovementView;

	private AddMovementControllerImpl(final AddMovementView addMovementView) {
		AddMovementControllerImpl.addMovementView = addMovementView;
		AddMovementControllerImpl.addMovementView.addBackListener(new BackListener());
		AddMovementControllerImpl.addMovementView.addInsertListener(new InsertListener());
	}

	public static AddMovementControllerImpl createAddMovementControllerImpl(final AddMovementView addMovementView) {
		return new AddMovementControllerImpl(addMovementView);
	}

	/**
	 * Back to previous page
	 */
	public class BackListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent arg0) {
			addMovementView.dispose();
			MoreFunctionsControllerImpl.createMoreFunctionsControllerImpl(
					MoreFunctionsView.createMoreFunctionsView(addMovementView.getRank()));
		}
	}

	/**
	 * Listener to insert movement
	 */
	public static class InsertListener implements ActionListener {

		@Override
		public void actionPerformed(final ActionEvent arg0) {
			MovementImpl movement = MovementImpl.createMovementImpl(addMovementView.getMovementDescription(),
					addMovementView.getMovementValue(), addMovementView.getMovementSymbol().charAt(0));

			if (movement.getAmount() <= 0) {
				addMovementView.displayErrorMessage(MyUtils.getInvalidInput());
				return;
			}

			List<MovementImpl> movementsList = new ArrayList<>();
			try {
				movementsList = getMovements();
			} catch (final IOException ioException) {
				ioException.printStackTrace();
			}
			movementsList.add(movement);
			try {
				setMovements(movementsList);
			} catch (final IOException exception) {
				exception.printStackTrace();
			}
			addMovementView.displayErrorMessage(MyUtils.getMovementInserted());
		}

		/**
		 * @return Movements' list
		 * @throws IOException
		 */
		public static List<MovementImpl> getMovements() throws IOException {
			File movementsFile = new File(MyUtils.getAllMovementsFile());
			if (movementsFile.length() == 0) {
				List<MovementImpl> movementsList = new ArrayList<>();
				return movementsList;
			}

			FileInputStream fileInputStream = new FileInputStream(movementsFile);
			ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

			List<MovementImpl> movementsList = new ArrayList<>();
			try {
				while (true) {
					MovementImpl actualMovement = (MovementImpl) objectInputStream.readObject();
					movementsList.add(actualMovement);
				}
			} catch (final EOFException endOfFileException) {
			} catch (final ClassNotFoundException classNotFoundException) {
				classNotFoundException.printStackTrace();
			} catch (final IOException exception) {
				exception.printStackTrace();
			}

			objectInputStream.close();
			return movementsList;
		}

		/**
		 * Set updated movements' list
		 * 
		 * @param movementsFile
		 *            Movement list
		 * @throws IOException
		 */
		public void setMovements(final List<MovementImpl> movementsFile) throws IOException {
			File movementFile = new File(MyUtils.getAllMovementsFile());
			FileOutputStream fileOutputStream = new FileOutputStream(movementFile);
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
			objectOutputStream.flush();
			fileOutputStream.flush();
			for (final MovementImpl movement : movementsFile) {
				objectOutputStream.writeObject(movement);
			}
			objectOutputStream.close();
		}
	}
}
