package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

import model.Interfaces.Prisoner;
import model.Interfaces.Visitor;
import utils.MyUtils;
import view.Interfaces.AddVisitorsView;
import view.Interfaces.MoreFunctionsView;

public class AddVisitorsControllerImpl {

	private static AddVisitorsView visitorsView;
	private static final int MIN_LENGTH_NAME_AND_SURNAME = 2;

	private AddVisitorsControllerImpl(final AddVisitorsView view) {
		AddVisitorsControllerImpl.visitorsView = view;
		visitorsView.addBackListener(new BackListener());
		visitorsView.addInsertVisitorListener(new InsertListener());
	}

	public static AddVisitorsControllerImpl createAddVisitorsControllerImpl(final AddVisitorsView view) {
		return new AddVisitorsControllerImpl(view);
	}

	/**
	 * Listener back to previous page
	 */
	public class BackListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			visitorsView.dispose();
			MoreFunctionsControllerImpl.createMoreFunctionsControllerImpl(
					MoreFunctionsView.createMoreFunctionsView(visitorsView.getRank()));
		}
	}

	/**
	 * Listener to manage insert visitors
	 */
	public static class InsertListener implements ActionListener {

		@Override
		public void actionPerformed(final ActionEvent arg0) {

			List<Visitor> visitorsList = new ArrayList<>();
			try {
				visitorsList = getVisitors();
			} catch (final ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}

			Visitor visitor = visitorsView.getVisitor();

			try {
				if (visitor.getName().length() < MIN_LENGTH_NAME_AND_SURNAME
						|| visitor.getSurname().length() < MIN_LENGTH_NAME_AND_SURNAME || !checkPrisonerID(visitor))
					visitorsView.displayErrorMessage(MyUtils.getCorrectDate());
				else {
					visitorsList.add(visitor);
					visitorsView.displayErrorMessage(MyUtils.getVisitorInserted());
				}
			} catch (final ClassNotFoundException classNotFoundException) {
				classNotFoundException.printStackTrace();
			} catch (final IOException ioException) {
				ioException.printStackTrace();
			}
			try {
				setVisitors(visitorsList);
			} catch (final IOException e) {
				e.printStackTrace();
			}

		}

		/**
		 * @return Visitors' list
		 * @throws IOException
		 * @throws ClassNotFoundException
		 */
		public static List<Visitor> getVisitors() throws IOException, ClassNotFoundException {
			File visitorsFile = new File(MyUtils.getVisitorsFile());
			if (visitorsFile.length() != 0) {
				FileInputStream fileInputStream = new FileInputStream(visitorsFile);
				ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
				List<Visitor> visitorsList = new ArrayList<>();
				try {
					while (true) {
						Visitor visitor = (Visitor) objectInputStream.readObject();
						visitorsList.add(visitor);
					}
				} catch (final EOFException eofe) {
				}
				fileInputStream.close();
				objectInputStream.close();
				return visitorsList;
			}
			return new ArrayList<Visitor>();
		}
	}

	/**
	 * Check prisoner's ID
	 * 
	 * @param visitor
	 *            Visitor
	 * @return true If ID is correct
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public static boolean checkPrisonerID(final Visitor visitor) throws ClassNotFoundException, IOException {
		List<Prisoner> prisonersList = MainControllerImpl.getCurrentPrisoners();
		boolean foundPrisoner = false;
		for (final Prisoner prisoner : prisonersList) {
			if (prisoner.getPrisonerID() == visitor.getPrisonerID()) {
				foundPrisoner = true;
				return foundPrisoner;
			} else
				continue;
		}
		return false;
	}

	/**
	 * sSet updated visitors' list
	 * 
	 * @param visitors
	 *            Visitors' list
	 * @throws IOException
	 */
	public static void setVisitors(final List<Visitor> visitors) throws IOException {
		File visitorsFile = new File(MyUtils.getVisitorsFile());
		FileOutputStream fileOutputStream = new FileOutputStream(visitorsFile);
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

		for (final Visitor visitor : visitors) {
			try {
				objectOutputStream.writeObject(visitor);
			} catch (final IOException ioException) {
				ioException.printStackTrace();
			}
		}

		objectOutputStream.close();
		fileOutputStream.close();
	}
}
