package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;

import controller.Interfaces.ViewPolicemenController;
import model.Interfaces.Policeman;
import utils.MyUtils;
import view.Interfaces.MoreFunctionsView;
import view.Interfaces.ViewPolicemanView;

public class ViewPolicemenControllerImpl implements ViewPolicemenController {

	private static ViewPolicemanView viewPolicemanView;
	private static final String PRISONER_VISITED = "Prisoner visited ID";
	private static final int NAME_POSITION = 0;
	private static final int SURNAME_POSITION = 1;
	private static final int BIRTHDAY_POSITION = 2;
	private static final int ID_POSITION = 3;

	private ViewPolicemenControllerImpl(final ViewPolicemanView viewPolicemanView) {
		ViewPolicemenControllerImpl.viewPolicemanView = viewPolicemanView;
		viewPolicemanView.addBackListener(new BackListener());
		viewPolicemanView.createPolicemenTable(getPolicemanTable());
	}

	public static ViewPolicemenControllerImpl createViewPolicemanControllerImpl(final ViewPolicemanView viewPolicemenView) {
		return new ViewPolicemenControllerImpl(viewPolicemenView);
	}

	/**
	 * Listener to back previous view
	 */
	public class BackListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent arg0) {
			viewPolicemanView.dispose();
			MoreFunctionsControllerImpl.createMoreFunctionsControllerImpl(
					MoreFunctionsView.createMoreFunctionsView(viewPolicemanView.getRank()));
		}
	}

	@Override
	public JTable getPolicemanTable() {
		List<Policeman> policemanList = new ArrayList<>();

		try {
			policemanList = AddPolicemenControllerImpl.InsertListener.getPoliceman();
		} catch (final ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}

		String[] policemanElements = { MyUtils.getName(), MyUtils.getSurname(), MyUtils.getBirthday(), PRISONER_VISITED };
		String[][] policemanMatrix = new String[policemanList.size()][policemanElements.length];
		for (int i = 0; i < policemanList.size(); i++) {
			policemanMatrix[i][NAME_POSITION] = policemanList.get(i).getName();
			policemanMatrix[i][SURNAME_POSITION] = policemanList.get(i).getSurname();
			policemanMatrix[i][BIRTHDAY_POSITION] = policemanList.get(i).getBirthdate().toString();
			policemanMatrix[i][ID_POSITION] = String.valueOf(policemanList.get(i).getPrisonerID());
		}
		JTable policemanTable = new JTable(policemanMatrix, policemanElements);
		return policemanTable;
	}

}
