package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import controller.Interfaces.ViewVisitorsController;
import model.Interfaces.Visitor;
import utils.MyUtils;
import view.Interfaces.MoreFunctionsView;
import view.Interfaces.ViewVisitorsView;

public class ViewVisitorsControllerImpl implements ViewVisitorsController {

	private static ViewVisitorsView viewVisitorsView;
	private static final String PRISONER_VISITED = "Prisoner visited ID";
	private static final int NAME_POSITION = 0;
	private static final int SURNAME_POSITION = 1;
	private static final int BIRTHDAY_POSITION = 2;
	private static final int ID_POSITION = 3;

	private ViewVisitorsControllerImpl(final ViewVisitorsView viewVisitorsView) {
		ViewVisitorsControllerImpl.viewVisitorsView = viewVisitorsView;
		viewVisitorsView.addBackListener(new BackListener());
		viewVisitorsView.createVisitorsTable(getVisitorsTable());
	}

	public static ViewVisitorsControllerImpl createViewVisitorsControllerImpl(final ViewVisitorsView viewVisitorsView) {
		return new ViewVisitorsControllerImpl(viewVisitorsView);
	}

	/**
	 * Listener to back previous view
	 */
	public class BackListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent arg0) {
			viewVisitorsView.dispose();
			MoreFunctionsControllerImpl.createMoreFunctionsControllerImpl(
					MoreFunctionsView.createMoreFunctionsView(viewVisitorsView.getRank()));
		}
	}

	@Override
	public JTable getVisitorsTable() {
		List<Visitor> visitorsList = new ArrayList<>();

		try {
			visitorsList = AddVisitorsControllerImpl.InsertListener.getVisitors();
		} catch (final ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}

		String[] visitorElements = { MyUtils.getName(), MyUtils.getSurname(), MyUtils.getBirthday(), PRISONER_VISITED };
		String[][] visitorMatrix = new String[visitorsList.size()][visitorElements.length];
		for (int i = 0; i < visitorsList.size(); i++) {
			visitorMatrix[i][NAME_POSITION] = visitorsList.get(i).getName();
			visitorMatrix[i][SURNAME_POSITION] = visitorsList.get(i).getSurname();
			visitorMatrix[i][BIRTHDAY_POSITION] = visitorsList.get(i).getBirthdate().toString();
			visitorMatrix[i][ID_POSITION] = String.valueOf(visitorsList.get(i).getPrisonerID());
		}
		JTable visitorsTable = new JTable(visitorMatrix, visitorElements);
		return visitorsTable;
	}

}
