package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;
import controller.Interfaces.InsertGuardController;
import model.Interfaces.Guard;
import utils.MyUtils;
import view.Interfaces.InsertGuardView;
import view.Interfaces.SupervisorFunctionsView;

public class InsertGuardControllerImpl implements InsertGuardController {

	private static InsertGuardView insertGuardView;
	private static final int MIN_RANK = 1;
	private static final int MAX_RANK = 3;
	private static final int PASSWORD_LENGTH = 6;

	private InsertGuardControllerImpl(final InsertGuardView insertGuardView) {
		InsertGuardControllerImpl.insertGuardView = insertGuardView;
		insertGuardView.addBackListener(new BackListener());
		insertGuardView.addInsertListener(new InsertListener());
	}

	public static InsertGuardControllerImpl createInsertGuardControllerImpl(final InsertGuardView insertGuardView) {
		return new InsertGuardControllerImpl(insertGuardView);
	}

	/**
	 * Back to the precedent page
	 */
	public class BackListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			insertGuardView.dispose();
			SupervisorControllerImpl.createSupervisorControllerImpl(
					SupervisorFunctionsView.createSupervisorFunctionsView(insertGuardView.getRank()));
		}
	}

	@Override
	public void insertGuard() throws ClassNotFoundException, IOException {
		List<Guard> guardsList = LoginControllerImpl.getGuards();
		Guard guardInserted = insertGuardView.getGuard();
		boolean containsGuard = false;

		for (final Guard guard : guardsList) {
			if (guard.getID() == guardInserted.getID()) {
				insertGuardView.displayErrorMessage(MyUtils.getIdAlreadyUsed());
				containsGuard = true;
			}
		}
		if (isSomethingEmpty(guardInserted)) {
			insertGuardView.displayErrorMessage(MyUtils.getCompleteFields());
			containsGuard = true;
		}
		if (containsGuard == false) {
			guardsList.add(guardInserted);
			setGuards(guardsList);
			insertGuardView.displayErrorMessage(MyUtils.getGuardInserted());

		}
	}

	/**
	 * Listener to insert guard
	 */
	public class InsertListener implements ActionListener {

		@Override
		public void actionPerformed(final ActionEvent arg0) {
			try {
				insertGuard();
			} catch (final ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean isSomethingEmpty(final Guard guard) {
		if (guard.getName().equals("") || guard.getSurname().equals("") || guard.getRank() < MIN_RANK
				|| guard.getRank() > MAX_RANK || guard.getID() < 0 || guard.getPassword().length() < PASSWORD_LENGTH)
			return true;
		return false;
	}

	/**
	 * Set guards
	 * 
	 * @param guardsList
	 *            Guards' list
	 * @throws IOException
	 */
	public static void setGuards(final List<Guard> guardsList) throws IOException {
		File guardsFile = new File(MyUtils.getGuardsFile());
		FileOutputStream fileOutputStream = new FileOutputStream(guardsFile);
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
		for (final Guard guard : guardsList) {
			objectOutputStream.writeObject(guard);
		}
		objectOutputStream.close();
		fileOutputStream.close();
	}
}
