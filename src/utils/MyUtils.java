package utils;

public class MyUtils {

	private static final String NAME = "Name";
	private static final String USERNAME = "Username";
	private static final String PASSWORD = "Password";
	private static final String TITLE_APP = "Prison Manager";
	private static final String DEFAULT_DATA = "01/01/1980";
	private static final String NEW_YEAR_DATA = "01/01/2018";
	private static final String SURNAME = "Surname";
	private static final String TELEPHONE_NUMBER = "Telephone number";
	private static final String BIRTHDAY = "Birthday";
	private static final String BIRTHDAY_PATTERN = "Birthday (mm/dd/yyy) ";
	private static final String START_PRISONING = "Start prisoning";
	private static final String END_PRISONING = "End prisoning";
	private static final String PRISONER_ID = "Prisoner ID";
	private static final String GUARD_ID = "Guard ID";
	private static final String INVALID_INPUT = "Invalid input";
	private static final String DATA_PATTERN = "MM/dd/yyyy";
	private static final String MOVEMENT_INSERTED = "Movement inserted";
	private static final String ALL_MOVEMENTS_FILE = "res/AllMovements.txt";
	private static final String ALL_POLICEMEN_FILE = "res/Policeman.txt";
	private static final String VISITOR_INSERTED = "Visitor inserted";
	private static final String CORRECT_DATE = "Insert correct date";
	private static final String VISITORS_FILE = "res/Visitors.txt";
	private static final String ELEMENT_SIGN = "+/-";
	private static final String ELEMENT_AMOUNT = "Amount";
	private static final String ELEMENT_DESCRIPTION = "Description";
	private static final String ELEMENT_DATE = "Date";
	private static final String PLUS = "+";
	private static final String MINUS = "-";
	private static final String CELL_ID = "Cell ID";
	private static final String MAX_CELL_CAPACITY = "Max cell capacity";
	private static final String ID_ALREADY_USED = "ID already used";
	private static final String COMPLETE_FIELDS = "Complete all fields";
	private static final String GUARD_INSERTED = "Guard inserted";
	private static final String GUARDS_FILE = "res/GuardsUserPass.txt";
	private static final String GUARD_NOT_FOUND = "Guard not found";
	private static final String CELLS_FILE = "res/Cells.txt";
	private static final String PRISONERS_FILE = "res/Prisoners.txt";
	private static final String ACTUAL_PRISONERS_FILE = "res/CurrentPrisoners.txt";
	private static final String ACTUAL_PRISONERS = "Actual prisoners";
	private static final String ANOTHER_CELL = "Try another cell";
	private static final String PRISONER_INSERTED = "Prisoner inserted";
	private static final String CRIME_ALREADY_PRESENT = "Crime already present";
	private static final String ADD_MOVEMENT = "Add movement";
	private static final String AMOUNT = "Amount";
	private static final String DESCRIPTION = "Description";
	private static final String BACK = "Back";
	private static final String INSERT_MOVEMENT = "Insert movement";
	private static final String INSERT = "Insert";
	private static final String BALANCE = "Balance: ";
	private static final int POSITION = 6;
	private static final String PRISONERS_FOR_YEAR = "Prisoners' number for year";
	private final static String CRIMES_AGAINST_ANIMALS = "Animals crimes";
	private final static String CRIMES_ASSOCIATIVES = "Associatives crimes";
	private final static String BLASPHEMY_AND_SACRILEGE = "Blasphemy and sacrilege";
	private final static String FINANCIAL_AND_ECONOMICS_CRIMES = "Financial and economics crimes";
	private final static String FALSE_TESTIMONY = "False testimony";
	private final static String MILITARY_CRIMES = "Military crimes";
	private final static String HERITAGE_CRIMES = "Heritage crimes";
	private final static String PERSON_CRIMES = "Person crimes";
	private final static String SYSTEM_CRIMES = "Italian legal system crimes";
	private final static String TAX_CRIMES = "Tax crimes";
	private final static String DRUG_CRIMES = "Drug crimes";
	private final static String SCAM_CASES = "Scam cases";
	private final static String PERCENT_CRIMES = "Percent of crimes made";

	public static String getInvalidInput() {
		return INVALID_INPUT;
	}

	public static String getMovementInserted() {
		return MOVEMENT_INSERTED;
	}

	public static String getAllMovementsFile() {
		return ALL_MOVEMENTS_FILE;
	}

	public static String getVisitorInserted() {
		return VISITOR_INSERTED;
	}

	public static String getCorrectDate() {
		return CORRECT_DATE;
	}

	public static String getVisitorsFile() {
		return VISITORS_FILE;
	}

	public static String getElementSign() {
		return ELEMENT_SIGN;
	}

	public static String getElementAmount() {
		return ELEMENT_AMOUNT;
	}

	public static String getElementDescription() {
		return ELEMENT_DESCRIPTION;
	}

	public static String getElementDate() {
		return ELEMENT_DATE;
	}

	public static String getPlus() {
		return PLUS;
	}

	public static String getMinus() {
		return MINUS;
	}

	public static String getIdAlreadyUsed() {
		return ID_ALREADY_USED;
	}

	public static String getCompleteFields() {
		return COMPLETE_FIELDS;
	}

	public static String getGuardInserted() {
		return GUARD_INSERTED;
	}

	public static String getGuardsFile() {
		return GUARDS_FILE;
	}

	public static String getCellsFile() {
		return CELLS_FILE;
	}

	public static String getPrisonersFile() {
		return PRISONERS_FILE;
	}

	public static String getAnotherCell() {
		return ANOTHER_CELL;
	}

	public static String getActualPrisonersFile() {
		return ACTUAL_PRISONERS_FILE;
	}

	public static String getPrisonerInserted() {
		return PRISONER_INSERTED;
	}

	public static String getCrimeAlreadyPresent() {
		return CRIME_ALREADY_PRESENT;
	}

	public static String getAddMovement() {
		return ADD_MOVEMENT;
	}

	public static String getAmount() {
		return AMOUNT;
	}

	public static String getDescription() {
		return DESCRIPTION;
	}

	public static String getBack() {
		return BACK;
	}

	public static String getInsertMovement() {
		return INSERT_MOVEMENT;
	}

	public static String getBalance() {
		return BALANCE;
	}

	public static String getInsert() {
		return INSERT;
	}

	public static String getName() {
		return NAME;
	}

	public static String getSurname() {
		return SURNAME;
	}

	public static String getPrisonerId() {
		return PRISONER_ID;
	}

	public static String getDataPattern() {
		return DATA_PATTERN;
	}

	public static String getCellId() {
		return CELL_ID;
	}

	public static String getDefaultData() {
		return DEFAULT_DATA;
	}

	public static int getPosition() {
		return POSITION;
	}

	public static String getNewYearData() {
		return NEW_YEAR_DATA;
	}

	public static String getPrisonersForYear() {
		return PRISONERS_FOR_YEAR;
	}

	public static String getCrimesAgainstAnimals() {
		return CRIMES_AGAINST_ANIMALS;
	}

	public static String getCrimesAssociatives() {
		return CRIMES_ASSOCIATIVES;
	}

	public static String getBlasphemyAndSacrilege() {
		return BLASPHEMY_AND_SACRILEGE;
	}

	public static String getFinancialAndEconomicsCrimes() {
		return FINANCIAL_AND_ECONOMICS_CRIMES;
	}

	public static String getFalseTestimony() {
		return FALSE_TESTIMONY;
	}

	public static String getMilitaryCrimes() {
		return MILITARY_CRIMES;
	}

	public static String getHeritageCrimes() {
		return HERITAGE_CRIMES;
	}

	public static String getPersonCrimes() {
		return PERSON_CRIMES;
	}

	public static String getSystemCrimes() {
		return SYSTEM_CRIMES;
	}

	public static String getTaxCrimes() {
		return TAX_CRIMES;
	}

	public static String getDrugCrimes() {
		return DRUG_CRIMES;
	}

	public static String getScamCases() {
		return SCAM_CASES;
	}

	public static String getPercentCrimes() {
		return PERCENT_CRIMES;
	}

	public static String getBirthday() {
		return BIRTHDAY;
	}

	public static String getStartPrisoning() {
		return START_PRISONING;
	}

	public static String getEndPrisoning() {
		return END_PRISONING;
	}

	public static String getMaxCellCapacity() {
		return MAX_CELL_CAPACITY;
	}

	public static String getActualPrisoners() {
		return ACTUAL_PRISONERS;
	}

	public static String getGuardNotFound() {
		return GUARD_NOT_FOUND;
	}

	public static String getGuardId() {
		return GUARD_ID;
	}

	public static String getBirthdayPattern() {
		return BIRTHDAY_PATTERN;
	}

	public static String getTelephoneNumber() {
		return TELEPHONE_NUMBER;
	}

	public static String getUsername() {
		return USERNAME;
	}

	public static String getPassword() {
		return PASSWORD;
	}

	public static String getTitleApp() {
		return TITLE_APP;
	}

	public static String getAllPolicemenFile() {
		return ALL_POLICEMEN_FILE;
	}
	
}
