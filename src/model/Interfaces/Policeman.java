package model.Interfaces;

public interface Policeman extends Person {

	/**
	 * @return ID of the visited prisoner
	 */
	int getPrisonerID();

	/**
	 * Set prisoner's ID
	 * 
	 * @param id
	 *            Prisoner's ID
	 */
	void setPrisonerID(final int prisonerID);
}
