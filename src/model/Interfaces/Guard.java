package model.Interfaces;

public interface Guard extends Person {

	/**
	 * Set guard's password
	 * 
	 * @param password
	 *            Guard's password
	 */
	void setPassword(final String password);

	/**
	 * @return Guard's password
	 */
	String getPassword();

	/**
	 * @return Guard's username
	 */
	int getUsername();

	/**
	 * @return Guard's telephone number
	 */
	String getTelephoneNumber();

	/**
	 * Set guard's telephone number
	 */
	void setTelephoneNumber(final String telephoneNumber);

	/**
	 * @return Guard's rank
	 */
	int getRank();

	/**
	 * Set guard's rank
	 */
	void setRank(final int rank);

	/**
	 * Set guard's ID
	 * 
	 * @param id
	 *            Guard's ID
	 */
	void setID(final int id);

	/**
	 * @return Guard's ID
	 */
	int getID();

}
