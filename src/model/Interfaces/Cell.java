package model.Interfaces;

public interface Cell {

	/**
	 * @return Cell's ID
	 */
	int getCellID();

	/**
	 * Set cell's ID
	 * 
	 * @param id
	 *            Cell's ID
	 */
	void setCellID(final int id);

	/**
	 * @return Cell's position
	 */
	String getPosition();

	/**
	 * Set cell's position
	 * 
	 * @param position
	 *            Cell's position
	 */
	void setPosition(final String position);

	/**
	 * @return Cell's max capacity
	 */
	int getCapacity();

	/**
	 * Set cell's max capacity
	 * 
	 * @param capacity
	 *            Cell's max capacitu
	 */
	void setCapacity(final int capacity);

	/**
	 * @return Cell's actual prisoners
	 */
	int getCurrentPrisoners();

	/**
	 * Set cell's prisoners
	 * 
	 * @param currentPrisoners
	 *            Cell's actual prisoners
	 */
	void setCurrentPrisoners(final int currentPrisoners);
}
