package model.Interfaces;

import java.util.Date;

public interface Person {

	/**
	 * @return Person's name
	 */
	String getName();

	/**
	 * @return Person's surname
	 */

	String getSurname();

	/**
	 * @return Person's birthdate
	 */

	Date getBirthdate();

	/**
	 * Set person's name
	 */
	void setName(final String name);

	/**
	 * Set person's surname
	 */

	void setSurname(final String surname);

	/**
	 * Set person's birthdate
	 */

	void setBirthdate(final Date birth);
	
}
