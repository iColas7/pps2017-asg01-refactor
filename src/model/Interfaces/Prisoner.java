package model.Interfaces;

import java.util.Date;
import java.util.List;

public interface Prisoner extends Person {

	/**
	 * Add crime in crimes' list
	 */
	void addCrime(final String crime);

	/**
	 * @return Crimes' list
	 */
	List<String> getCrimes();

	/**
	 * @return Prisoner's ID
	 */
	int getPrisonerID();

	/**
	 * Set prisoner's ID
	 */
	void setPrisonerID(final int prisonerID);

	/**
	 * @return Start prisoning
	 */
	Date getStartPrisoning();

	/**
	 * Set start prisoning
	 */
	void setStartPrisoning(final Date startPrisoning);

	/**
	 * @return End prisoning
	 */
	Date getEndPrisoning();

	/**
	 * Set end prisoning
	 */
	void setEndPrisoning(final Date endPrisoning);

	/**
	 * @return Cell's ID
	 */
	int getCellID();

	/**
	 * Set cell's ID
	 * 
	 * @param Cell's
	 *            ID
	 */
	void setCellID(final int cellID);
}