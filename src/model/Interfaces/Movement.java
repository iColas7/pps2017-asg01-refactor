package model.Interfaces;

public interface Movement {

	/**
	 * 
	 * @return Description of the movement
	 */
	String getMovementDescription();

	/**
	 * Set description of the movement
	 * 
	 * @param Description
	 *            of the movement
	 */
	void setMovementDescription(final String descr);

	/**
	 * @return Smount of the movement
	 */
	double getAmount();

	/**
	 * Set movement's quantity
	 * 
	 * @param Amount
	 *            of the movement
	 */
	void setAmount(final double amount);

	/**
	 * @return The sign of the movement (+ or -)
	 */
	char getPlusOrMinusMovement();

	/**
	 * @return The date of the movement
	 */
	String getMovementDate();

}
