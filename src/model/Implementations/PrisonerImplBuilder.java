package model.Implementations;

import java.util.Date;
import java.util.List;

public class PrisonerImplBuilder {
   
	private String name;
    private String surname;
    private Date birthdate;
    private int prisonerID;
    private Date startPrisoning;
    private Date endPrisoning;
    private List<String> listOfCrimes;
    private int cellID;

    public PrisonerImplBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public PrisonerImplBuilder setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public PrisonerImplBuilder setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
        return this;
    }

    public PrisonerImplBuilder setPrisonerID(int prisonerID) {
        this.prisonerID = prisonerID;
        return this;
    }

    public PrisonerImplBuilder setStartPrisoning(Date startPrisoning) {
        this.startPrisoning = startPrisoning;
        return this;
    }

    public PrisonerImplBuilder setEndPrisoning(Date endPrisoning) {
        this.endPrisoning = endPrisoning;
        return this;
    }

    public PrisonerImplBuilder setListOfCrimes(List<String> listOfCrimes) {
        this.listOfCrimes = listOfCrimes;
        return this;
    }

    public PrisonerImplBuilder setCellID(int cellID) {
        this.cellID = cellID;
        return this;
    }

    public PrisonerImpl createPrisonerImpl() {
        return PrisonerImpl.createPrisonerImpl(name, surname, birthdate, prisonerID, startPrisoning, endPrisoning, listOfCrimes, cellID);
    }
}