package model.Implementations;

import java.util.Date;

import model.Interfaces.Policeman;

public class PolicemanImpl extends PersonImpl implements Policeman {

	private static final long serialVersionUID = 5306827736761721189L;
	private int prisonerID;

	private PolicemanImpl(final String name, final String surname, final Date birthdate, final int prisonerID) {
		super(name, surname, birthdate);
		this.prisonerID = prisonerID;
	}

	public static PolicemanImpl createPolicemanImpl(final String name, final String surname, final Date birthdate,
			final int prisonerID) {
		return new PolicemanImpl(name, surname, birthdate, prisonerID);
	}

	@Override
	public int getPrisonerID() {
		return this.prisonerID;
	}

	@Override
	public void setPrisonerID(final int prisonerID) {
		this.prisonerID = prisonerID;
	}

	@Override
	public String toString() {
		return "PolicemanImpl Name=" + getName() + ", Surname=" + getSurname() + ", Birthdate=" + getBirthdate() + "[ID="
				+ getPrisonerID() + "]";
	}

}
