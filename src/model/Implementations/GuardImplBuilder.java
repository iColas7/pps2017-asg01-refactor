package model.Implementations;

import java.util.Date;

public class GuardImplBuilder {
	private String guardName;
	private String guardSurname;
	private Date guardBirthdate;
	private int guardRank;
	private String guardTelephoneNumber;
	private int guardId;
	private String guardPassword;

	public GuardImplBuilder setName(final String name) {
		this.guardName = name;
		return this;
	}

	public GuardImplBuilder setSurname(final String surname) {
		this.guardSurname = surname;
		return this;
	}

	public GuardImplBuilder setBirthdate(final Date birthdate) {
		this.guardBirthdate = birthdate;
		return this;
	}

	public GuardImplBuilder setRank(final int rank) {
		this.guardRank = rank;
		return this;
	}

	public GuardImplBuilder setTelephoneNumber(final String telephoneNumber) {
		this.guardTelephoneNumber = telephoneNumber;
		return this;
	}

	public GuardImplBuilder setGuardId(final int guardId) {
		this.guardId = guardId;
		return this;
	}

	public GuardImplBuilder setPassword(final String password) {
		this.guardPassword = password;
		return this;
	}

	public GuardImpl createGuardImpl() {
		return GuardImpl.createGuardImpl(guardName, guardSurname, guardBirthdate, guardRank, guardTelephoneNumber,
				guardId, guardPassword);
	}
}