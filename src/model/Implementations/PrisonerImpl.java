package model.Implementations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.Interfaces.Prisoner;

public class PrisonerImpl extends PersonImpl implements Prisoner {

	private static final long serialVersionUID = -3204660779285410481L;
	private int prisonerID;
	private Date startPrisoning;
	private Date endPrisoning;
	private List<String> listOfCrimes = new ArrayList<>();
	private int cellID;

	private PrisonerImpl(final String name, final String surname, final Date birthdate, final int prisonerID,
			final Date startPrisoning, final Date endPrisoning, final List<String> listOfCrimes, final int cellID) {
		super(name, surname, birthdate);
		this.prisonerID = prisonerID;
		this.startPrisoning = startPrisoning;
		this.endPrisoning = endPrisoning;
		this.listOfCrimes = listOfCrimes;
		this.cellID = cellID;
	}

	public static PrisonerImpl createPrisonerImpl(final String name, final String surname, final Date birthdate,
			final int prisonerID, final Date startPrisoning, final Date endPrisoning, final List<String> listOfCrimes,
			final int cellID) {
		return new PrisonerImpl(name, surname, birthdate, prisonerID, startPrisoning, endPrisoning, listOfCrimes,
				cellID);
	}

	@Override
	public void addCrime(final String crime) {
		listOfCrimes.add(crime);
	}

	@Override
	public List<String> getCrimes() {
		return this.listOfCrimes;
	}

	@Override
	public int getPrisonerID() {
		return this.prisonerID;
	}

	@Override
	public void setPrisonerID(final int prisonerID) {
		this.prisonerID = prisonerID;
	}

	@Override
	public Date getStartPrisoning() {
		return this.startPrisoning;
	}

	@Override
	public void setStartPrisoning(final Date startPrisoning) {
		this.startPrisoning = startPrisoning;
	}

	@Override
	public Date getEndPrisoning() {
		return this.endPrisoning;
	}

	@Override
	public void setEndPrisoning(final Date endPrisoning) {
		this.endPrisoning = endPrisoning;
	}

	@Override
	public int getCellID() {
		return cellID;
	}

	@Override
	public void setCellID(final int cellID) {
		this.cellID = cellID;
	}

	@Override
	public String toString() {
		return "PrisonerImpl [PrisonerID=" + prisonerID + ", StartPrisoning=" + startPrisoning + ", EndPrisoning="
				+ endPrisoning + ", Crimes=" + listOfCrimes + "]";
	}

}
