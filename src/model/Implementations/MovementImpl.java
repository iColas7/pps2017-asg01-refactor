package model.Implementations;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import model.Interfaces.Movement;

public class MovementImpl implements Serializable, Movement {

	private static final long serialVersionUID = -8676396152502823263L;
	private String movementDescription;
	private double amount;
	private SimpleDateFormat date;
	private Calendar calendar;
	private String movementDate;
	private char plusOrMinusMovement;

	private MovementImpl(final String description, final double amount, final char plusOrMinusMovement) {
		this.amount = amount;
		this.movementDescription = description;
		this.date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		this.calendar = Calendar.getInstance();
		this.movementDate = date.format(calendar.getTime());
		this.plusOrMinusMovement = plusOrMinusMovement;
	}

	public static MovementImpl createMovementImpl(final String description, final double amount,
			final char plusOrMinusMovement) {
		return new MovementImpl(description, amount, plusOrMinusMovement);
	}

	@Override
	public String getMovementDescription() {
		return this.movementDescription;
	}

	@Override
	public void setMovementDescription(String descr) {
		this.movementDescription = descr;
	}

	@Override
	public double getAmount() {
		return this.amount;
	}

	@Override
	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Override
	public char getPlusOrMinusMovement() {
		return this.plusOrMinusMovement;
	}

	@Override
	public String getMovementDate() {
		return this.movementDate;
	}

	public String toString() {
		return "Movement [Description=" + movementDescription + "+/-" + plusOrMinusMovement + ", Amount=" + amount
				+ ", Date=" + movementDate + "]";
	}
}
