package model.Implementations;

import java.util.Date;
import model.Interfaces.Visitor;

public class VisitorImpl extends PersonImpl implements Visitor {

	private static final long serialVersionUID = 5306827736761721189L;
	private int prisonerID;

	private VisitorImpl(final String name, final String surname, final Date birthdate, final int prisonerID) {
		super(name, surname, birthdate);
		this.prisonerID = prisonerID;
	}

	public static VisitorImpl createVisitorImpl(final String name, final String surname, final Date birthdate,
			final int prisonerID) {
		return new VisitorImpl(name, surname, birthdate, prisonerID);
	}

	@Override
	public int getPrisonerID() {
		return this.prisonerID;
	}

	@Override
	public void setPrisonerID(final int prisonerID) {
		this.prisonerID = prisonerID;
	}

	@Override
	public String toString() {
		return "VisitorImpl Name=" + getName() + ", Surname=" + getSurname() + ", Birthdate=" + getBirthdate() + "[ID="
				+ getPrisonerID() + "]";
	}

}
