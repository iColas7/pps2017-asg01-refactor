package model.Implementations;

import java.io.Serializable;
import java.util.Date;

import model.Interfaces.Person;

public class PersonImpl implements Serializable, Person {

	private static final long serialVersionUID = -6542753884631178660L;

	private String name;
	private String surname;
	private Date birthdate;

	PersonImpl(final String name, final String surname, final Date birthdate) {
		super();
		this.name = name;
		this.surname = surname;
		this.birthdate = birthdate;
	}

	public static PersonImpl createPersonImpl(final String name, final String surname, final Date birthdate) {
		return new PersonImpl(name, surname, birthdate);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(final String name) {
		this.name = name;
	}

	@Override
	public String getSurname() {
		return surname;
	}

	@Override
	public void setSurname(final String surname) {
		this.surname = surname;
	}

	@Override
	public Date getBirthdate() {
		return birthdate;
	}

	@Override
	public void setBirthdate(final Date birthdate) {
		this.birthdate = birthdate;
	}

	@Override
	public String toString() {
		return "PersonImpl [Name=" + this.getName() + ", Surname=" + this.getSurname() + ", Birthdate="
				+ this.getBirthdate() + "]";
	}

}
