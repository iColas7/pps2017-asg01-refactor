package model.Implementations;

import java.util.Date;
import model.Interfaces.Guard;

public class GuardImpl extends PersonImpl implements Guard {

	private static final long serialVersionUID = 3975704240795357459L;
	private int guardRank;
	private String guardTelephoneNumber;
	private int guardID;
	private String guardPassword;

	private GuardImpl(final String name, final String surname, final Date birthdate, final int rank,
			final String telephoneNumber, final int id, final String password) {
		super(name, surname, birthdate);
		this.setRank(rank);
		this.setTelephoneNumber(telephoneNumber);
		this.guardID = id;
		this.guardPassword = password;
	}

	public static GuardImpl createGuardImpl(final String name, final String surname, final Date birthdate,
			final int rank, final String telephoneNumber, final int id, final String password) {
		return new GuardImpl(name, surname, birthdate, rank, telephoneNumber, id, password);
	}

	@Override
	public void setPassword(final String password) {
		this.guardPassword = password;
	}

	@Override
	public String getPassword() {
		return this.guardPassword;
	}

	@Override
	public int getUsername() {
		return this.guardID;
	}

	@Override
	public String getTelephoneNumber() {
		return this.guardTelephoneNumber;
	}

	@Override
	public void setTelephoneNumber(final String telephoneNumber) {
		this.guardTelephoneNumber = telephoneNumber;
	}

	@Override
	public int getRank() {
		return this.guardRank;
	}

	@Override
	public void setRank(final int rank) {
		this.guardRank = rank;
	}

	@Override
	public int getID() {
		return this.guardID;
	}

	@Override
	public void setID(final int guardId) {
		this.guardID = guardId;
	}

	@Override
	public String toString() {
		return "GuardImpl [Rank=" + guardRank + ", TelephoneNumber=" + guardTelephoneNumber + ", ID=" + guardID + "]";
	}

}
