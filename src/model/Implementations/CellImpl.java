package model.Implementations;

import java.io.Serializable;
import model.Interfaces.Cell;

public class CellImpl implements Cell, Serializable {

	private static final long serialVersionUID = 9167940013424894676L;
	private int cellID;
	private String cellPosition;
	private int cellCapacity;
	private int cellCurrentPrisoners;

	private CellImpl(final int id, final String position, final int capacity) {
		this.cellID = id;
		this.cellPosition = position;
		this.cellCapacity = capacity;
		this.cellCurrentPrisoners = 0;
	}

	public static CellImpl createCellImpl(final int id, final String position, final int capacity) {
		return new CellImpl(id, position, capacity);
	}

	@Override
	public int getCellID() {
		return cellID;
	}

	@Override
	public void setCellID(final int id) {
		this.cellID = id;
	}

	@Override
	public String getPosition() {
		return cellPosition;
	}

	@Override
	public void setPosition(final String position) {
		this.cellPosition = position;
	}

	@Override
	public int getCapacity() {
		return cellCapacity;
	}

	@Override
	public void setCapacity(final int capacity) {
		this.cellCapacity = capacity;
	}

	@Override
	public int getCurrentPrisoners() {
		return cellCurrentPrisoners;
	}

	@Override
	public void setCurrentPrisoners(final int currentPrisoners) {
		this.cellCurrentPrisoners = currentPrisoners;
	}

	@Override
	public String toString() {
		return "CellImpl [ID=" + cellID + ", Position=" + cellPosition + ", Capacity=" + cellCapacity
				+ ", CurrentPrisoners=" + cellCurrentPrisoners + "]";
	}

}
