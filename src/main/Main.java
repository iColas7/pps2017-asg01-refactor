package main;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import controller.Implementations.LoginControllerImpl;
import model.Implementations.CellImpl;
import model.Implementations.GuardImpl;
import model.Implementations.GuardImplBuilder;
import utils.MyUtils;
import view.Interfaces.LoginView;

public final class Main {

	private static final String DATA = "res";
	private static final int START = 0;
	private static final int TOTAL_FLOOR = 50;
	private static final int LIMIT_FIRST_FLOOR = 20;
	private static final int LIMIT_SECOND_FLOOR = 40;
	private static final int LIMIT_THIRD_FLOOR = 45;
	private static final int ONE_PLACE_IN_CELL = 1;
	private static final int THREE_PLACES_IN_CELL = 3;
	private static final int FOUR_PLACES_IN_CELL = 4;
	private static final int FIRST_RANK = 1;
	private static final int SECOND_RANK = 2;
	private static final int THIRD_RANK = 3;
	private static final String NAME_FIRST_FLOR = "First floor";
	private static final String NAME_SECOND_FLOR = "Second floor";
	private static final String NAME_THIRD_FLOR = "Third floor";
	private static final String NAME_SECRET_FLOOR = "Secret floor, isolation cells";

	/**
	 * Program main, this is the "root" of the application.
	 * 
	 * @param args
	 *            unused,ignore
	 * @throws IOException
	 * @throws ParseException
	 */
	public static void main(final String... args) throws IOException, ParseException {
		createDataDirectory();
		createGuardsFile();
		createCellsFile();
		LoginControllerImpl.createLoginControllerImpl(LoginView.createLoginView());
	}

	 private static void initializeCells(final File cellsFile) throws IOException {
		List<CellImpl> cellsList = new ArrayList<>();
		CellImpl cell;

		for (int i = START; i < TOTAL_FLOOR; i++) {
			if (i < LIMIT_FIRST_FLOOR) {
				cell = CellImpl.createCellImpl(i, NAME_FIRST_FLOR, FOUR_PLACES_IN_CELL);
			} else if (i < LIMIT_SECOND_FLOOR) {
				cell = CellImpl.createCellImpl(i, NAME_SECOND_FLOR, THREE_PLACES_IN_CELL);
			} else if (i < LIMIT_THIRD_FLOOR) {
				cell = CellImpl.createCellImpl(i, NAME_THIRD_FLOR, FOUR_PLACES_IN_CELL);
			} else {
				cell = CellImpl.createCellImpl(i, NAME_SECRET_FLOOR, ONE_PLACE_IN_CELL);
			}
			cellsList.add(cell);
		}
		
		writeStreamFile(cellsFile, cellsList);
	}

	 private static void initializeGuards(final File guardsFile) throws IOException, ParseException {
		Date date = getDate();
		List<GuardImpl> guardsList = new ArrayList<>();
		GuardImpl firstGuard = new GuardImplBuilder().setName("Oronzo").setSurname("Cantani").setBirthdate(date)
				.setRank(FIRST_RANK).setTelephoneNumber("0764568").setGuardId(1).setPassword("ciao01").createGuardImpl();
		guardsList.add(firstGuard);
		GuardImpl secondGuard = new GuardImplBuilder().setName("Emile").setSurname("Heskey").setBirthdate(date)
				.setRank(SECOND_RANK).setTelephoneNumber("456789").setGuardId(2).setPassword("asdasd").createGuardImpl();
		guardsList.add(secondGuard);
		GuardImpl thirdGuard = new GuardImplBuilder().setName("Gennaro").setSurname("Alfieri").setBirthdate(date)
				.setRank(THIRD_RANK).setTelephoneNumber("0764568").setGuardId(3).setPassword("qwerty").createGuardImpl();
		guardsList.add(thirdGuard);
		
		writeStreamFile(guardsFile, guardsList);
	}

	private static void createDataDirectory() {
		String dataDirectory = DATA;
		new File(dataDirectory).mkdir();
	}

	private static void createGuardsFile() throws IOException, ParseException {
		File guards = new File(MyUtils.getGuardsFile());
		if (guards.length() == START) {
			initializeGuards(guards);
		}
	}

	private static void createCellsFile() throws IOException {
		File cells = new File(MyUtils.getCellsFile());
		if (cells.length() == START) {
			initializeCells(cells);
		}
	}

	private static Date getDate() throws ParseException {
		String datePattern = MyUtils.getDataPattern();
		SimpleDateFormat dateFormat = new SimpleDateFormat(datePattern);
		Date date = dateFormat.parse(MyUtils.getDefaultData());
		return date;
	}
	
	private static void writeStreamFile (final File fileToWrite, final List<?> listObject) throws IOException {
		FileOutputStream outputStreamFile = new FileOutputStream(fileToWrite);
		ObjectOutputStream objectOutputStreamFile = new ObjectOutputStream(outputStreamFile);
		objectOutputStreamFile.flush();
		outputStreamFile.flush();
		
		for (final Object entity : listObject) {
			objectOutputStreamFile.writeObject(entity);
		}
		
		objectOutputStreamFile.close();
	}

}
